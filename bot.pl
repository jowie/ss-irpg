#!/usr/local/bin/perl -w

use strict;
use warnings;
use IO::Socket;
use IO::Select;
use Data::Dumper;
use Getopt::Long;
use POSIX qw(ceil floor);

my %opts;

readconfig();

my $version = "4.4.1";

# command line overrides .irpg.conf
GetOptions(\%opts,
        "help|h",
        "debug",
        "debugfolder=s",
        "server|s=s",
        "serverarena|sa=s",
        "ignorelist",
        "botnick|n=s",
        "botpasswd|p=s",
        "botchat|c=s",
        "mirrorchat",
        "botentercmd|o=s",
        "helpurl=s",
        "admincommurl=s",
        "silentmode=i",
        "writequestfile",
        "questfilename=s",
        "mapurl=s",
        "statuscmd",
        "pidfile=s",
        "reconnect",
        "reconnect_wait=i",
        "maxqueue",
        "self_clock=i",
        "modsfile=s",
        "allowuserinfo",
        "noscale",
        "owner=s",
        "owneralwaysonline",
        "owneraddonly",
        "ownerdelonly",
        "limitpen=i",
        "mapx=i",
        "mapy=i",
        "okurl|k=s@",
        "eventsfile=s",
        "rpstep=f",
        "rpbase=i",
        "rppenstep=f",
        "chatlogfolder",
        "gamelogfolder",
        "arenalogfolder",
        "dbfile|irpgdb|db|d=s",
        "disablestats",
        "duelminimumlevel",
        "actionpointsperlevel",
        "actionextrapointtime",
        "actionmaxpoints",
        "canspam"
) or debug("Error: Could not parse command line. Try $0 --help\n",1);

$opts{help} and do { help(); exit 0; };

debug("Config: read $_: ".Dumper($opts{$_})) for keys(%opts);

use constant MAXMESSAGELENGTH => 220;
use constant LOGNICKLENGTH => 30;
use constant WARBIRD    => 1;
use constant JAVELIN    => 2;
use constant SPIDER     => 3;
use constant LEVIATHAN  => 4;
use constant TERRIER    => 5;
use constant WEASEL     => 6;
use constant LANCASTER  => 7;
use constant SHARK      => 8;
use constant SPECTATOR  => 9;

my $outbytes = 0; # sent bytes
my $primnick = $opts{botnick}; # for regain or register checks
my $inbytes = 0; # received bytes
my %onarena; # users on game channel
my %ignore; # Ignored nick names
my %rps; # role-players
my %quest = (
        questers => [],
        p1 => [], # point 1 for q2
        p2 => [], # point 2 for q2
        qtime => time() + int(rand(21600)), # first quest starts in <=6 hours
        text => "",
        type => 1,
        stage => 1); # quest info

my $rpreport = 0;
my $pausemode = 0; # pausemode on/off flag
my $silentmode = int($opts{silentmode}); # silent mode 0/1/2/3
my @queue; # outgoing message queue
my $lastreg = 0; # holds the time of the last reg. cleared every second.
                                 # prevents more than one account being registered / second
my $registrations = 0; # count of registrations this period
my $sel; # IO::Select object
my $lasttime = 1; # last time that rpcheck() was run
my $buffer; # buffer for socket stuff
my $conn_tries = 0; # number of connection tries. gives up after trying each
                                        # server twice
my $sock; # IO::Socket::INET object
my $freemessages = $opts{canspam} ? 50 : 4; # number of "free" privmsgs we can send. 0..$freemessages
my $lastmessage_received = 0; # time()
my $lastmessage_received_noop = 0;
my $lastmessage_sent = 0; # time()

my $chatCommandSent = 0;       # When was ?chat sent, this set to 0 after having received a response
my $chatCommandReplyLines = 0; # How many lines of ?chat readout has been received so far
my $chatCommandLastReply = 0;  # When was the last reply to ?chat
my %onchat;                    # People on chat during last ?chat readout
my @onchat_buffer;             # Used to store nicks temporarily in the middle of a ?chat readout

my %monsters;
$monsters{"Green_Dragon"}{level} = 200;
$monsters{"Green_Dragon"}{sum} = int(rand(10000)+1000);
$monsters{"Green_Dragon"}{regen} = 2;
$monsters{"Red_Dragon"}{level} = 250;
$monsters{"Red_Dragon"}{sum} = int(rand(15000)+2000);
$monsters{"Red_Dragon"}{regen} = 2;
$monsters{"Black_Dragon"}{level} = 300;
$monsters{"Black_Dragon"}{sum} = int(rand(20000)+3000);
$monsters{"Black_Dragon"}{regen} = 2;

my $jowie_spree = 0;

my @possible_items = ( 
        "ring", "amulet", "charm", "weapon", "helm",
        "tunic", "pair of gloves", "shield",
        "set of leggings", "pair of boots"
);


my %special_items = (
        "a" => "Joris' Omniscience Grand Crown",
        "b" => "Hat's Protectorate Electronic Mail",
        "c" => "CDB's Storm Magic Amulet",
        "d" => "Irvel's Furry Colossal Sword",
        "e" => "Neostar's Cane of Blind Rage",
        "f" => "Hi-5's Magical Boots of Swiftness",
        "g" => "JoWie's Cluehammer of Doom",
        "h" => "Beam's Glorious Ring of Sparkliness",
        "i" => "Noldec' Silk Skirt of Ancient Magick",
        "j" => "Jokerd3's Charm of Blight",
        "k" => "IdleRPG's Magic Cheat",
        "m" => "Stone of Jordan",
);

sub daemonize(); # prototype to avoid warnings

use constant DB_FIELDS => 34;

if (! -e $opts{dbfile}) 
{
        $|=1;
        %rps = ();
        print "$opts{dbfile} does not appear to exist. I'm guessing this is your ".
                  "first time using IRPG. Please give an account name that you would ".
                  "like to have admin access [$opts{owner}]: ";
        chomp(my $uname = <STDIN>);
        $uname =~ s/\s.*//g;
        $uname = length($uname)?$uname:$opts{owner};
        $rps{lc($uname)}{username} = $uname;
        $uname = lc($uname);
        print "Enter a character class for this account: ";
        chomp(my $uclass = <STDIN>);
        $rps{$uname}{class} = substr($uclass,0,30);
        print "Enter a password for this account: ";
        if ($^O ne "MSWin32") {
                system("stty -echo");
        }
        chomp(my $upass = <STDIN>);
        if ($^O ne "MSWin32") {
                system("stty echo");
        }
        $rps{$uname}{pass} = crypt($upass,mksalt());
        $rps{$uname}{TTL} = $opts{rpbase};
        $rps{$uname}{next} = $rps{$uname}{TTL};
        $rps{$uname}{nick} = "";
        $rps{$uname}{level} = 0;
        $rps{$uname}{loggedin} = 0;
        $rps{$uname}{idled} = 0;
        $rps{$uname}{created} = time();
        $rps{$uname}{lastseen} = time();
        $rps{$uname}{lastactivity} = time();
        $rps{$uname}{joinedarena} = time();
        $rps{$uname}{x} = int(rand($opts{mapx}));
        $rps{$uname}{y} = int(rand($opts{mapy}));
        $rps{$uname}{alignment}="n";
        $rps{$uname}{isadmin} = 1;
        $rps{$uname}{stat_combat} = 0;
        $rps{$uname}{stat_strength} = 0;
        $rps{$uname}{stat_endurance} = 0;
        $rps{$uname}{stat_points} = 10;
        $rps{$uname}{actionpoints} = 0;
        $rps{$uname}{nextbonusactionpoint} = $opts{actionextrapointtime};
        $rps{$uname}{gender} = "m";

        for my $item (@possible_items) 
        {
                $rps{$uname}{item}{$item} = 0;
        }
        writedb();
        print "OK, wrote you into $opts{dbfile}.\n";
}

print "\n".alllog("*** IdleRPG for Chatnet starting...")."\n";

#if ($^O ne "MSWin32")
#{
#        print "\n".debug("Becoming a daemon...")."\n";
#        daemonize();
#}

$SIG{HUP} = "readconfig"; # sighup = reread config file
$SIG{INT} = 'cleanup';
$SIG{TERM} = 'cleanup';

CONNECT: # cheese.
$sock = 0;
$lastmessage_received = 0;
$lastmessage_received_noop = 0;
$lastmessage_sent = 0;

$primnick = $opts{botnick};

loaddb();
readignorelist();

#while (!$sock && $conn_tries < 2*@{$opts{servers}}) 
while (!$sock) #keep trieng
{
        alllog("*** Connecting to $opts{servers}->[0]...");
        my %sockinfo = (PeerAddr => $opts{servers}->[0], PeerPort => 6667);
        if ($opts{localaddr}) { $sockinfo{LocalAddr} = $opts{localaddr}; }
        $sock = IO::Socket::INET->new(%sockinfo) or
                debug("Error: failed to connect: $!\n");
        ++$conn_tries;
        if (!$sock) 
        {
                # cycle front server to back if connection failed
                push(@{$opts{servers}},shift(@{$opts{servers}}));
        }
        else { debug("Connected."); }
}

if (!$sock) 
{
        alllog("*** Error: Too many connection failures, exhausted server list.\n",1);
}

$conn_tries = 0;

$sel = IO::Select->new($sock);

sts("LOGIN:1;IdleRPG, JoWie, pm HELP:$opts{botnick}:$opts{botpasswd}", 1);

while (1) 
{
        my($readable) = IO::Select->select($sel,undef,undef,0.5);
        if (defined($readable)) 
        {
                my $fh = $readable->[0];
                my $buffer2;
                $fh->recv($buffer2,512,0);
                if (length($buffer2)) {
                    # received something
                    $lastmessage_received = time();
                    $lastmessage_received_noop = 0;
                        $buffer .= $buffer2;
                        while (index($buffer,"\n") != -1) {
                                my $line = substr($buffer,0,index($buffer,"\n")+1);
                                $buffer = substr($buffer,length($line));
                                parse($line);
                        }
                }
                else
                {
                        writedb();

                        close($fh);
                        $sel->remove($fh);

                        if ($opts{reconnect}) 
                        {
                                undef(@queue);
                                undef($sock);
                                alllog("*** Socket closed; disconnected. Cleared outgoing message ".
                                          "queue. Waiting $opts{reconnect_wait}s before next ".
                                          "connection attempt...");
                                sleep($opts{reconnect_wait});
                                goto CONNECT;
                        }
                        else { alllog("*** Socket closed; disconnected.",1); }
                }
        }
        else { select(undef,undef,undef,1); }
        if ((time()-$lasttime) >= $opts{self_clock}) { rpcheck(); }
}


sub parse 
{
        my($in) = shift;
        $inbytes += length($in); # increase parsed byte count
        $in =~ s/[\r\n]//g; # strip all \r and \n
        debug("<- $in");

        my ($type, $remaining, $squad, $msg, $arenaname, $freq, $nick, $ship, $username, $killer, $killed, $bounty, $flagscarried, $subtype, $chnum, $otheruser, $otheruserlc); 
        
        ($type, $remaining) = split(':', $in, 2);
        $type = lc($type);

        if ($chatCommandSent && $chatCommandReplyLines) 
        {
                
                if ($type eq 'msg')
                {
                        $msg = $remaining;
                        
                        unless (length($opts{botchat}) > 0 && $msg =~ /^ARENA:$opts{botchat}: .*$/i)
                        {
                                # Received something that is not part of the chat readout, stop gathering info
                                $chatCommandSent = 0;
                                $chatCommandReplyLines = 0;
                                undef %onchat;
                                
                                $onchat{lc($_)} = 1 foreach (@onchat_buffer);
                                delete($onchat{$_}) foreach (keys %ignore);
                                undef @onchat_buffer;
                        }
                }
                else
                {       # Received something that is not part of the chat readout, stop gathering info
                
                        $chatCommandSent = 0;
                        $chatCommandReplyLines = 0;
                        undef %onchat;
                        
                        $onchat{lc($_)} = 1 foreach (@onchat_buffer);
                        delete($onchat{$_}) foreach(keys %ignore);
                        
                        undef @onchat_buffer;
                }
        }

        if ($type eq 'loginbad')
        { # LOGINBAD:message
                $msg = $remaining;

                debug("Login bad: $msg");
                writedb();
                return;
        }
        elsif ($type eq 'loginok') 
        { # LOGINOK:yourname
                $msg = $remaining;
                $primnick = $msg;
                sts("GO:$opts{serverarena}");
        }
        elsif ($type eq 'inarena') 
        { # INARENA:arenaname:freq
                ($arenaname, $freq) = split(':', $remaining, 2);
                
                $lasttime = time(); # start rpcheck()
                undef %onarena;
                if (length($opts{botchat}) > 0) 
                {
                        #sts("SEND:CMD:?chat=$opts{botchat},$opts{mirrorchat}");
                        sts("SEND:CMD:?chat=$opts{botchat}");
                        sendchatcmd();
                }
                
                foreach (keys %rps) 
                {
                        $rps{$_}{joinedarena} = 0;
                }
                
                (my $botentercmd = $opts{botentercmd}) =~ s/%botnick%/$opts{botnick}/eg;
                sts($botentercmd);
        }
        elsif ($type eq 'entering') 
        { # ENTERING:name:ship:freq
                ($nick, $ship, $freq) = split(':', $remaining, 3);
        
                $nick = lc($nick);
        
                if (length($nick) > 0)
                {
                        return if ($ignore{$nick});
                        $onarena{$nick} = $ship+1;
                        
                        $username = finduser($nick);
                        
                        # do not store last activity; The entry may be automated
                        
                        if ($username)
                        {
                                $rps{$username}{joinedarena} = time();
                                
                                if (isonline($username))
                                {
                                        privmsg("Welcome back $rps{$username}{username}, the level $rps{$username}{level} $rps{$username}{class}! ".
                                        "Message me with LOGOUT if you want to stop playing. Don't forget to join ?chat $opts{botchat}", $nick);
                                }
                        }
                }
        }
        elsif ($type eq 'leaving')
        { # LEAVING:name
                $nick = lc($remaining);
                return if ($ignore{$nick});
                $username = finduser($nick);
                
                if (defined($username))
                {
                        $rps{$username}{joinedarena} = 0;
                }
                $onarena{$nick} = 0;
                delete($onarena{$nick});
        }
        elsif ($type eq 'shipfreqchange') 
        { # SHIPFREQCHANGE:name:ship:freq
                ($nick, $ship, $freq) = split(':', $remaining, 3);
                $nick = lc($nick);
                return if ($ignore{$nick} || !length($nick));
                $onarena{$nick} = $ship + 1;
                
                $username = finduser($nick);
                if (defined($username))
                {
                        $rps{$username}{lastactivity} = time();
                }
                
                if ($nick eq 'jowie')
                {
                        $jowie_spree = 0;
                }
        }
        elsif ($type eq 'kill') 
        { # KILL:killername:killedname:bounty:flagscarried
                ($killer, $killed, $bounty, $flagscarried) = split(':', $remaining, 4);
                
                my $killerlc = lc($killer);
                my $killedlc = lc($killed);
        
                $username = finduser($killerlc);
                if (defined($username))
                {
                        $rps{$username}{lastactivity} = time();
                }
                
                $username = finduser($killedlc);
                if (defined($username))
                {
                        $rps{$username}{lastactivity} = time();
                }
                
#                 if ($killedlc eq 'jowie')
#                 {
#                         $jowie_spree = 0;
#                 }
#                 
#                 if ($killerlc eq 'jowie')
#                 {
#                         $jowie_spree++;
#                         
#                         my $reward = 90 + ($jowie_spree * 10);
#                         
#                         $reward = 1000 if ($reward > 1000);
# 
#                         #sts("SEND:PRIV:$killed:?give $reward Killed by JoWie (". $jowie_spree . ")");
#                         sts("SEND:PRIV:$killed:?give $reward");
#                 }
        }
        elsif ($type eq 'player') 
        { #PLAYER:name:ship:freq
                ($nick, $ship, $freq) = split(':', $remaining, 3);
                $nick = lc($nick);
                
                if ($nick)
                {
                        return if ($ignore{$nick});
                        $onarena{$nick} = $ship + 1;
                        
                        $username = finduser($nick);
                        
                        if ($username)
                        {
                                $rps{$username}{joinedarena} = time();
                        }
                }
        }
        elsif ($type eq 'msg') 
        {       
                ($subtype, $remaining) = split(':', $remaining, 2);
                $subtype = lc($subtype);
                
                if ($subtype eq 'arena')
                {
                        $msg = $remaining;
                        
                        if (length($opts{botchat}) > 0 && $msg =~ /^$opts{botchat}: (.*)$/i)
                        {
                                my $nicks = trim($1);
                                
                                if ($chatCommandSent && (time() - $chatCommandSent < 20 || $chatCommandReplyLines)) #?chat was sent less then 20 secons ago, no timeout if it is not the first message
                                {
                                
                                        $chatCommandReplyLines++;
                                        if ($chatCommandReplyLines == 1)
                                        {
                                                undef @onchat_buffer;
                                        }
                                        
                                        my @found_nicks = split(',', $nicks);
                                        if (@found_nicks)
                                        {
                                                @onchat_buffer = (@onchat_buffer, @found_nicks);
                                        }
                                        $chatCommandLastReply = time();
                                }
                                else    # no ?chat command was sent (or it was to long ago)
                                {
                                        $chatCommandSent = 0;
                                        $chatCommandReplyLines = 0;
                                        undef @onchat_buffer;
                                }
                        }
                        else
                        {
                                arenalog('A '.$msg);
                        }
                        
                        # Reconnect when the biller is back up
                        # Message taken from ASSS 1.4.4 module billing_ssc. Unfortunally the module billing does not use such messages
                        if ($msg eq 'Notice: Connection to user database server restored. Log in again for full functionality')
                        {       
                                writedb();
                                close($sock);
                                sleep(3);
                                goto CONNECT;
                                return;
                        }
                }
                elsif ($subtype eq 'priv' || $subtype eq 'squad') 
                { # MSG:PRIV:name:msg
                  # MSG:SQUAD:squad:sender:msg
                        if ($subtype eq 'priv')
                        {
                                ($nick, $msg) = split(':', $remaining, 2);
                        }
                        elsif ($subtype eq 'squad')
                        {
                                ($squad, $nick, $msg) = split(':', $remaining, 3);
                        }
                        $msg = trim($msg);
                        
                        $msg =~ s/^[!\.]//;
                        
                        my ($command) = split(' ', $msg, 2);                        
                        $command = trim(lc($command));

                        my $nicklc = lc($nick);
                        return if ($ignore{$nicklc});
                        $username = finduser($nicklc);
                        
                        if (defined($username))
                        {
                                $rps{$username}{lastactivity} = time();
                        }
                        
                        if (lc($primnick) eq $nicklc)
                        {
                                return;
                        }
                        
                        if ($silentmode == 2 && !ha($username))
                        {
                                return;
                        }
                        
                        if ($silentmode == 3 && $command ne "silent")
                        {
                                return;
                        }
                        
                        
                        if ($command eq "noop")
                        {
                                return;
                        }
                        
                        gamelog('P ' . prependSpaces($nick, LOGNICKLENGTH)."> $msg");
                        
                        if ($command eq "register") 
                        {
                                if (defined $username) 
                                {
                                        cmdresponse("Sorry, you are already online as $username.",$nick);
                                }
                                elsif ($pausemode)
                                {
                                        cmdresponse("Sorry, new accounts may not be registered ".
                                                        "while the bot is in pause mode; please wait ".
                                                        "a few minutes and try again.",$nick);
                                }
                                else 
                                {
                                    if ($msg =~ /^register\s+([^\:]+)\s*:\s*([^\:]+)\s*:\s*([^\:]+)\s*$/i) 
                                    {           
                                                my $charname = trim($1);
                                                my $charpassword = trim($2);
                                                my $charclass = trim($3);

                                                $charname =~ s/:/ /g;
                                                $charname =~ s/\s+/ /g; #double spaces -> single space
                                                my $charnamelc = lc($charname);
                                                
                                                $charclass =~ s/:/ /g;
                                                $charclass =~ s/\s+/ /g;
                                                
                                                
                                                if (exists $rps{$charnamelc}) 
                                                {
                                                        cmdresponse("Sorry, that character name is already in use.", $nick);
                                                }
#                                                 elsif (!$onarena{$nicklc}) 
#                                                 {
#                                                         cmdresponse("Sorry, you're not in the same arena", $nick);
#                                                 }
                                                elsif ($charnamelc eq lc($opts{botnick}) ||
                                                           $charnamelc eq lc($primnick)) {
                                                        cmdresponse("Sorry, that character name cannot be ".
                                                                        "registered.",$nick);
                                                }
                                                elsif (length($charname) > 20 || length($charname) < 1) {
                                                        cmdresponse("Sorry, character names must be < 21 and > 0 ".
                                                                        "chars long.", $nick);
                                                }
                                                elsif (index($charname, '#', 0) == 0)
                                                {
                                                        cmdresponse("Sorry, character names may not begin with #.",        $nick);
                                                }
                                                elsif (index($charname, '\001', 0) != -1 ) 
                                                {
                                                        cmdresponse("Sorry, neither character names nor classes may include non-printable chars.",$nick);
                                                }
                                                elsif (($charname =~ /[^a-zA-Z0-9 ]/ ||
                                                           $charclass =~ /[^a-zA-Z0-9 ]/)) {
                                                        cmdresponse("Sorry, your character name or class contains invalid chars.",$nick);
                                                }
                                                elsif (length($charclass) > 30 || length($charclass) < 1) {
                                                        cmdresponse("Sorry, character classes must be < 31 and > 0 chars long.",$nick);
                                                }
                                                elsif (time() <= $lastreg + 30) {
                                                        cmdresponse("Someone else just registered. Wait up to 30 seconds and try again.", $nick);
                                                }
                                                elsif ($charname =~ /n[i1l]gg([e3]r|[a@])|n[e3]gr[o0]/i) {
                                                        cmdresponse("Sorry, your character name or class contains invalid chars.",$nick);
                                                }
                                                else 
                                                {
                                                        if ($charclass =~ /n[i1l]gg([e3]r|[a@])|n[e3]gr[o0]/i)
                                                        {
                                                                $charclass = "retard";
                                                        }
                                                
                                                        ++$registrations;
                                                        $lastreg = time();
                                                        $rps{$charnamelc}{username} = $charname;
                                                        $rps{$charnamelc}{TTL} = $opts{rpbase};
                                                        $rps{$charnamelc}{next} = $rps{$charnamelc}{TTL};
                                                        $rps{$charnamelc}{class} = $charclass;
                                                        $rps{$charnamelc}{level} = 0;
                                                        $rps{$charnamelc}{loggedin} = 1;
                                                        $rps{$charnamelc}{nick} = $nicklc;
                                                        $rps{$charnamelc}{created} = time();
                                                        $rps{$charnamelc}{lastseen} = time();
                                                        $rps{$charnamelc}{lastactivity} = time();
                                                        $rps{$charnamelc}{pass} = crypt($charpassword,mksalt());
                                                        $rps{$charnamelc}{x} = int(rand($opts{mapx}));
                                                        $rps{$charnamelc}{y} = int(rand($opts{mapy}));
                                                        $rps{$charnamelc}{alignment}="n";
                                                        $rps{$charnamelc}{isadmin} = 0;
                                                        $rps{$charnamelc}{stat_combat} = 0;
                                                        $rps{$charnamelc}{stat_strength} = 0;
                                                        $rps{$charnamelc}{stat_endurance} = 0;
                                                        $rps{$charnamelc}{stat_points} = 10;
                                                        $rps{$charnamelc}{actionpoints} = 0;
                                                        $rps{$charnamelc}{gender} = "m";
                                                        $rps{$charnamelc}{nextbonusactionpoint} = $opts{actionextrapointtime};
                                                        for my $item (@possible_items) {
                                                                $rps{$charnamelc}{item}{$item} = 0;
                                                        }

                                                        cmdresponse("Success! Account $charname created. You have ".
                                                                        "$opts{rpbase} seconds idleness until you ".
                                                                        "reach level 1. Make sure to join ?chat ".$opts{botchat}, $nick);
                                                        chatmsg("Welcome $nick\'s new player $charname, the ".
                                                                        "$charclass! Next level in ".
                                                                        duration($opts{rpbase}).".");
                                                }
                                        } 
                                        else 
                                        {
                                                cmdresponse("Try: REGISTER Character Name:Recovery Password:Character Class", $nick);
                                                cmdresponse("Example: REGISTER Poseidon:MyPassword:God of the Sea", $nick);
                                        }
                                }
                        }
                        elsif ($command eq "cmdlist")
                        {
                                cmdresponse("REGISTER DUEL CLASS GENDER QUEST STATUS WHOAMI TOP FINDUSER STATS STATRESET ADDSTAT ITEMS NEWPASS RECOVER SEX SHEEP ALIGN HELP INFO LOGIN SEEN POP NOOP", $nick);
                        }
                        elsif (($command eq "delold") && ha($username)) {
                                # delold <days>
                                # insure it is a number
                                if ($msg =~ /^delold\s+([\d\.]+)\s*$/i) {
                                        my @oldaccounts = grep 
                                        { 
                                                (time()-$rps{$_}{lastseen}) > ($1 * 86400) &&
                                                !isonline($_)
                                        } keys(%rps);
                                                
                                        delete(@rps{@oldaccounts});
                                        chatmsg(scalar(@oldaccounts)." accounts not accessed in ".
                                                        "the last $1 days removed by $nick.");
                                }
                                else 
                                {
                                        cmdresponse("Try: DELOLD <# of days> Example: DELOLD 14.3", $nick);
                                }
                        }
                        elsif (($command eq "del") && (ha($username))) 
                        {
                                if ($msg =~ /^del\s+([^\:]+)\s*$/i) {
                                        my $u = lc($1);
                                        if (!exists($rps{$u}))
                                        {
                                                cmdresponse("No such account $1.", $nick);
                                        }
                                        else 
                                        {
                                                chatmsg("Account $rps{$u}{username} removed by $nick.");
                                                delete($rps{$u});
                                        }
                                }
                                else {
                                        cmdresponse("Try: DEL <char name>", $nick);
                                }
                        }
                        elsif (($command eq "mkadmin") && ((ha($username) && !$opts{owneraddonly}) || ($opts{owneraddonly} && ($opts{owner} eq $username)))) 
                        {
                                if ($msg =~ /^mkadmin\s+([^\:]+)\s*$/i) 
                                {
                                        my $u = lc($1);
                                        if (!exists($rps{$u})) 
                                        {
                                                cmdresponse("No such account $1.", $nick);
                                        }
                                        else 
                                        {
                                                $rps{$u}{isadmin} = 1;
                                                cmdresponse("Account $1 is now a bot admin.",$nick);
                                        }
                                }
                                else {
                                        cmdresponse("Try: MKADMIN <char name>", $nick);
                                }
                        }
                        elsif (($command eq "raw") && ((ha($username) && !$opts{owneraddonly}) || ($opts{owneraddonly} && ($opts{owner} eq $username)))) 
                        {
                                if ($msg =~ /^raw\s+(.*)$/i) 
                                {
                                        sts($1);
                                }
                                else
                                {
                                        cmdresponse("Try: RAW <chatnet command>", $nick);
                                }
                        }
                        elsif ($command eq "chatmsg" && ha($username))
                        {
                                if ($msg =~ /^chatmsg\s+(.*)$/i) 
                                {
                                        chatmsg($1);
                                }
                                else
                                {
                                        cmdresponse("Try: CHATMSG <message>", $nick);
                                }
                        }
                        elsif (($command eq "deladmin") && ((ha($username) && !$opts{owneraddonly}) || ($opts{owneraddonly} && ($opts{owner} eq $username)))) 
                        {
                                if ($msg =~ /^deladmin\s+([^\:]+)\s*$/i) 
                                {
                                        my $u = lc($1);
                                        if (!exists($rps{$u})) 
                                        {
                                                cmdresponse("No such account $1.", $nick);
                                        }
                                        elsif ($u eq $opts{owner}) 
                                        {
                                                cmdresponse("Cannot DELADMIN owner account.", $nick);
                                        }
                                        else 
                                        {
                                                $rps{$u}{isadmin} = 0;
                                                cmdresponse("Account $rps{$u}{username} is no longer a bot admin.", $nick);
                                        }
                                }
                                else {
                                        cmdresponse("Try: DELADMIN <char name>", $nick);
                                }
                        }
                        elsif (($command eq "chpass") && ha($username))
                        {
                                if ($msg =~ /^chpass\s+([^\:]+)\s*:\s*([^\:]+)\s*$/i)
                                {
                                        my $u = lc($1);
                                        my $newpass = trim($2);
                                        if (!exists($rps{$u})) 
                                        {
                                            cmdresponse("No such username $1.", $nick);
                                        }
                                        else 
                                        {
                                            $rps{$u}{pass} = crypt($newpass,mksalt());
                                            cmdresponse("Password for $rps{$u}{username} changed.", $nick);
                                        }
                                }
                                else 
                                {
                                       cmdresponse("Try: CHPASS <char name>:<new pass>", $nick);
                                }
                        }
                        elsif (($command eq "chuser") && ha($username)) 
                        {
                                if ($msg =~ /^chuser\s+([^\:]+)\s*:\s*([^\:]+)\s*$/i) 
                                {
                                        my $u = lc($1);
                                        my $u2 = lc($2);
                                        
                                        if (!exists($rps{$u})) 
                                        {
                                                cmdresponse("No such username $1.", $nick);
                                        }
                                        elsif (exists($rps{$u2})) 
                                        {
                                                cmdresponse("Username $2 is already taken.", $nick);
                                        }
                                        else 
                                        {
                                                $rps{$u2} = delete($rps{$u});
                                                $rps{$u2}{username} = $2;
                                                cmdresponse("Username for $1 changed to $2.", $nick);
                                        }
                                }
                                else 
                                {
                                        cmdresponse("Try: CHUSER <char name>:<new char name>", $nick);
                                }
                        }
                        elsif (($command eq "pit") && ha($username)) {
                                if ($msg =~ /^pit\s+([^\:]+)\s*:\s*([^\:]+)\s*$/i) 
                                {
                                        my $u = lc($1);
                                        my $u2 = lc($2);
                                        
                                        if (!exists($rps{$u}) && $u ne lc($primnick)) 
                                        {
                                                cmdresponse("No such username $1.", $nick);
                                        }
                                        elsif (!exists($rps{$u2}) && $u2 ne lc($primnick)) 
                                        {
                                                cmdresponse("No such username $2.", $nick);
                                        }
                                        else 
                                        {
                                                challenge_opp($u, $u2);
                                        }
                                }
                                elsif ($msg =~ /^pit\s+([^\:]+)\s*$/i)
                                {
                                        my $u = lc($1);
                                        if (!exists($rps{$u}) && $u ne lc($primnick)) 
                                        {
                                                cmdresponse("No such username $1.", $nick);
                                        }
                                        else 
                                        {
                                                challenge_opp($u);
                                        }
                                }
                                else 
                                {
                                        cmdresponse("Try: PIT <char name>:<opponent char name>", $nick);
                                }
                        }
                        elsif ($command eq "duel" || $command eq "d")
                        {
                                if ($msg =~ /^(?:duel|d)\s+([^\:]+)\s*$/i) 
                                {
                                        # (?: ) <- does not get stored in $x
                                        # (argument is optional)
                                        $otheruser = lc($1);
                                        
                                        if (!defined($username)) 
                                        {
                                                cmdresponse("You are not logged in", $nick);
                                        }
                                        elsif ($rps{$username}{level} < $opts{duelminimumlevel})
                                        {
                                                cmdresponse("You must be at least level $opts{duelminimumlevel} to duel someone", $nick);
                                        }
                                        elsif (!exists($rps{$otheruser})) 
                                        {
                                                cmdresponse("No such user", $nick);
                                        }
                                        elsif ($otheruser eq $username)
                                        {
                                                cmdresponse("You have been banned", $nick);
                                        }
                                        elsif (!isonline($otheruser))
                                        {
                                                cmdresponse("User is not online", $nick);
                                        }
                                        else
                                        {
                                                if ($rps{$username}{actionpoints} > 0)
                                                {
                                                        if ($rps{$otheruser}{level} < $opts{duelminimumlevel})
                                                        {
                                                                cmdresponse("Your opponent must be at least level $opts{duelminimumlevel}. Your opponent is level ".$rps{$otheruser}{level}, $nick);
                                                        }
                                                        else
                                                        {
                                                                duel($username, $otheruser);
                                                        }
                                                }
                                                else
                                                {
                                                        cmdresponse("You have used up all your action points", $nick);
                                                }        
                                        }
                                }
                                else
                                {
                                        cmdresponse("Try: DUEL <char name>", $nick);
                                }
                        }
                        elsif ($command eq "slay" && 0)
                        {
                                if ($msg =~ /^slay\s+([^\:]+)\s*$/i)
                                {
                                        if (!defined($username)) 
                                        {
                                                cmdresponse("You are not logged in", $nick);
                                        }
                                        elsif ($rps{$username}{level} < 30) 
                                        {
                                                cmdresponse("You must be atleast level 30 to attempt a slay", $nick);
                                        }
                                        elsif (!exists($monsters{$1})) 
                                        {
                                                cmdresponse("That monster does not exist! Try Green Dragon or Red Dragon or Black Dragon", $nick);
                                        }
                                        elsif ($rps{$username}{slaytm} > time()) 
                                        {
                                                my $slaytm = $rps{$username}{slaytm}-time();
                                                cmdresponse("You are not yet fully recovered from your last slay, please ".
                                                            "wait ".duration($slaytm).".", $nick);
                                        }
                                        else 
                                        {
                                                slay_fight($username, $1);
                                        }
                                }
                                else
                                {
                                        cmdresponse("Try: SLAY <Green Dragon/Red Dragon/Black Dragon>", $nick);
                                } 
                        }
                        elsif ($command eq "class")
                        {
                                if ($msg =~ /^class\s+(.+)$/i) 
                                {
                                        my $class = trim($1);
                                        $class =~ s/\s+/ /g;
                                        
                                        if (length($class) > 30)
                                        {
                                                cmdresponse("Sorry, character classes must be < 31 chars long.", $nick);
                                        }
                                        elsif (($class =~ /[^a-zA-Z0-9 ]/))
                                        {
                                                cmdresponse("Sorry, your character class contains invalid chars.",$nick);
                                        }
                                        else
                                        {
                                                $rps{$username}{class} = $class;
                                                if ($class =~ /n[i1l]gg([e3]r|[a@])|n[e3]gr[o0]/i)
                                                {
                                                        $class = "retard";
                                                }
                                                cmdresponse("Your class has been changed to $class", $nick);
                                                chatmsg("$rps{$username}{username} has changed his class to $class");
                                                penalize($username, "classchange");
                                        }
                                }
                                else
                                {
                                        cmdresponse("Try: CLASS <New Character Class>", $nick);
                                }
                        }
                        elsif (($command eq "chclass") && ha($username)) {
                                if ($msg =~ /^chclass\s+([^\:]+)\s*:\s*(.+)$/i) 
                                {
                                        my $u = lc($1);
                                        if (!exists($rps{$u})) 
                                        {
                                                cmdresponse("No such username $1.", $nick);
                                        }
                                        else 
                                        {
                                                $rps{$u}{class} = $2;
                                                cmdresponse("Class for $rps{$u}{username} changed to $2.", $nick);
                                                chatmsg("The class of $rps{$u}{username} was changed to $2");
                                        }
                                }
                                else 
                                {
                                        cmdresponse("Try: CHCLASS <char name>:<new char class>", $nick);
                                }
                        }
                        elsif (($command eq "push") && ha($username)) {
                                # insure it's a positive or negative, integral number of seconds
                                if ($msg =~ /^push\s+([^\:]+)\s*:\s*(\-?\d+)\s*$/i) 
                                {
                                        my $u = lc($1);
                                        
                                        if (!exists($rps{$u})) 
                                        {
                                                cmdresponse("No such username $1.", $nick);
                                        }
                                        elsif ($2 > $rps{$u}{next}) 
                                        {
                                                #cmdresponse("Time to level for $1 ($rps{$1}{next}s) ".
                                                #                "is lower than $2; setting TTL to 0.",
                                                #                $nick);
                                                chatmsg("$nick has pushed $rps{$u}{username} $rps{$u}{next} ".
                                                                "seconds toward level ".($rps{$u}{level}+1));
                                                $rps{$u}{next} = 0;
                                        }
                                        else 
                                        {
                                                $rps{$u}{next} -= $2;
                                                 chatmsg("$nick has pushed $rps{$u}{username} $2 seconds ".
                                                                 "toward level ".($rps{$u}{level}+1).". ".
                                                                 "$rps{$u}{username} reaches next level in ".
                                                                 duration($rps{$u}{next}).".");
                                        }
                                }
                                else {
                                        cmdresponse("Try: PUSH <char name>:<seconds>", $nick);
                                }
                        }
                        elsif (($command eq "setlevel") && ha($username)) 
                        {
                                if ($msg =~ /^setlevel\s+([^\:]+)\s*:\s*(\d+)\s*$/i) 
                                {
                                        my $u = lc($1);
                                        if (!exists($rps{$u})) 
                                        {
                                                cmdresponse("No such username $1.", $nick);
                                        }
                                        else 
                                        {
                                                $rps{$u}{level} = $2;
                                                chatmsg("$nick has set $rps{$u}{username} to level $rps{$1}{level}.");
                                        }
                                }
                                else {
                                        cmdresponse("Try: SETLEVEL <char name>:<level>", $nick);
                                }
                        }
                        elsif (($command eq "addlevel") && ha($username))
                        {
                                if ($msg =~ /^addlevel\s+([^\:]+)\s*:\s*(\d+)\s*$/i) 
                                {
                                        my $u = lc($1);
                                        if (!exists($rps{$u})) 
                                        {
                                                cmdresponse("No such username $1.", $nick);
                                        }
                                        else 
                                        {
                                                $rps{$u}{level} += $2;
                                                $rps{$u}{stat_points} += $2;
                                                for (my $a = 0; $a < $2; $a++)
                                                {
                                                        find_item($u, undef, 1);
                                                }
                                                chatmsg("$nick has added $2 levels to $rps{$u}{username} and is now level $rps{$u}{level}");
                                        }
                                }
                                else {
                                        cmdresponse("Try: ADDLEVEL <char name>:<levels>", $nick);
                                }
                        }
                        elsif (($command eq "item")  && ha($username)) 
                        {
                                if ($msg =~ /^item\s+([^\:]+)\s*$/i) 
                                {
                                        my $u = lc($1);
                                               
                                        if (!exists($rps{$u}))
                                        {
                                                cmdresponse("No such account $1.", $nick);
                                        }
                                        else 
                                        {
                                                cmdresponse("Finding an item for $rps{$u}{username}.", $nick);
                                                
                                                #TODO: specifiy item type
                                                #"ring", "amulet", "charm", "weapon", "helm",
                                                #"tunic", "pair of gloves", "shield",
                                                #"set of leggings", "pair of boots"
                                                
                                                find_item($u);
                                        }
                                }
                                else 
                                {
                                        cmdresponse("Try: ITEM <char name>", $nick);
                                }
                        }
                        elsif (($command eq "setp")  && ha($username)) 
                        {
                                if ($msg =~ /^setp\s+([^\:]+)\:([^\:]+):(\w+)$/i) 
                                {
                                        my $u = lc($1);
                                        my $name = $2;
                                        my $value = $3;
                                               
                                        if (!exists($rps{$u}))
                                        {
                                                cmdresponse("No such account $1.", $nick);
                                        }
                                        else 
                                        {
                                                my $oldValue = $rps{$u}{$name};
                                                $oldValue = 'UNDEF' unless (defined($oldValue));
                                                $rps{$u}{$name} = $value;
                                                cmdresponse("$name = $oldValue -> $value", $nick);
                                        }
                                }
                                else 
                                {
                                        cmdresponse("Try: SETP <char name>:<name>:<value>", $nick);
                                }
                        }
                        elsif (($command eq "getp")  && ha($username)) 
                        {
                                if ($msg =~ /^getp\s+([^\:]+)\:([^\:]+)$/i)
                                {
                                        my $u = lc($1);
                                        my $name = $2;
                                               
                                        if (!exists($rps{$u}))
                                        {
                                                cmdresponse("No such account $1.", $nick);
                                        }
                                        else 
                                        {
                                                cmdresponse("$name = ".$rps{$u}{$name}, $nick);
                                        }
                                }
                                else 
                                {
                                        cmdresponse("Try: GETP <char name>:<name>", $nick);
                                }
                        }
                        elsif (($command eq "setitem")  && ha($username)) 
                        {
                                if ($msg =~ /^setitem\s+([^\:]+)\:([^\:]+):(\w+)$/i) 
                                {
                                        my $u = lc($1);
                                        my $name = $2;
                                        my $value = $3;
                                               
                                        if (!exists($rps{$u}))
                                        {
                                                cmdresponse("No such account $1.", $nick);
                                        }
                                        elsif (!exists($rps{$u}{item}{$name}))
                                        {
                                                cmdresponse("No such item", $nick);
                                        }
                                        else 
                                        {
                                                my $oldValue = $rps{$u}{item}{$name};
                                                $rps{$u}{item}{$name} = $value;
                                                cmdresponse("$name = $oldValue -> $value", $nick);
                                        }
                                }
                                else 
                                {
                                        cmdresponse("Try: SETITEM <char name>:<item name>:<value>", $nick);
                                }
                        }
                        elsif (($command eq "getitem")  && ha($username)) 
                        {
                                if ($msg =~ /^getitem\s+([^\:]+)\:([^\:]+)$/i)
                                {
                                        my $u = lc($1);
                                        my $name = $2;
                                               
                                        if (!exists($rps{$u}))
                                        {
                                                cmdresponse("No such account $1.", $nick);
                                        }
                                        elsif (!exists($rps{$u}{item}{$name}))
                                        {
                                                cmdresponse("No such item", $nick);
                                        }
                                        else 
                                        {
                                                cmdresponse("$name = ".$rps{$u}{item}{$name}, $nick);
                                        }
                                }
                                else 
                                {
                                        cmdresponse("Try: GETITEM <char name>:<item name>", $nick);
                                }
                        }
                        elsif ($command eq "logout") 
                        {
                                if (defined($username)) 
                                {
                                        cmdresponse("You are now logged out.", $nick);
                                        penalize($username, "logout");
                                        $rps{$username}{loggedin} = 0;
                                        chatmsg ("$rps{$username}{username} has logged out");
                                }
                                else 
                                {
                                        cmdresponse("You are not logged in.", $nick);
                                }
                        }
                        elsif ($command eq "quest") {
                                if (!@{$quest{questers}}) {
                                        cmdresponse("There is no active quest.",$nick);
                                }
                                elsif ($quest{type} == 1) {
                                        cmdresponse("$rps{$quest{questers}->[1]}{username}, $rps{$quest{questers}->[2]}{username} and ".
                                                        "$rps{$quest{questers}->[3]}{username} are on a quest to ".
                                                        "$quest{text}. Quest to complete in ".
                                                        duration($quest{qtime}-time()).".",$nick);
                                }
                                elsif ($quest{type} == 2) {
                                        cmdresponse("$rps{$quest{questers}->[1]}{username}, $rps{$quest{questers}->[2]}{username} and ".
                                                        "$rps{$quest{questers}->[3]}{username} are on a quest to ".
                                                        "$quest{text}. Participants must first reach ".
                                                        "[$quest{p1}->[0],$quest{p1}->[1]], then ".
                                                        "[$quest{p2}->[0],$quest{p2}->[1]].".
                                                        ($opts{mapurl}?" See $opts{mapurl} to monitor ".
                                                        "their journey's progress.":""),$nick);
                                }
                        }
                        elsif ($command eq "newquest" && ha($username))
                        {
                                cmdresponse("Starting a new quest if enough users are online...",$nick);
                                quest(60);
                        }
                        elsif (($command eq "status" || $command eq "s") && ($opts{statuscmd} || ha($username)))
                        {
                                if ($msg =~ /^(?:status|s)(?:\s+([^\:]+)\s*)?$/i) 
                                {
                                # (?: ) <- does not get stored in $x
                                # (argument is optional)
                                
                                        if ($1 && !exists($rps{lc($1)})) 
                                        {
                                                cmdresponse("No such user.",$nick);
                                        }
                                        elsif ($1) 
                                        { # optional 'user' argument
                                                my $u = lc($1);
                                                my $align;
                                                if ($rps{$u}{alignment} eq 'g') { $align = "good"; } 
                                                elsif ($rps{$u}{alignment} eq 'e') { $align = "evil"; } 
                                                else { $align = "neutral"; }
                                                
                                                my $sum = itemsum($u, 1);
                                                my $minroll = ($opts{disablestats} ? int($sum * 0.1) : int($sum * ($rps{$u}{stat_combat} / 50)));
                                                my $maxroll = ($opts{disablestats} ? $sum : int($sum * (1 + $rps{$u}{stat_strength} / 50)));
                                                
                                                cmdresponse("$rps{$u}{username} ($rps{$u}{nick}): Level $rps{$u}{level} ".
                                                                "$rps{$u}{class}; Status: O".
                                                                (isonline($u)?"n":"ff")."line; ".
                                                                "Alignment: $align; ".
                                                                "TTL: ".duration($rps{$u}{next})."; ".
                                                                "Idled: ".duration($rps{$u}{idled}).
                                                                "; Item sum: ".itemsum($u,0)." ($minroll - $maxroll); ".
                                                                "Actions: " .$rps{$u}{actionpoints}. "; ".
                                                                ($opts{disablestats} ? '' : "Stats: $rps{$u}{stat_points} free, $rps{$u}{stat_combat} combat, $rps{$u}{stat_strength} strength, $rps{$u}{stat_endurance} endurance; ")
                                                                ,$nick);
                                        }
                                        else 
                                        { # no argument, look up this user
                                                if (!defined($username)) 
                                                {
                                                        cmdresponse("You are not logged in.", $nick);
                                                }
                                                else
                                                {
                                                        my $align;
                                                        if ($rps{$username}{alignment} eq 'g') { $align = "good"; } 
                                                        elsif ($rps{$username}{alignment} eq 'e') { $align = "evil"; } 
                                                        else { $align = "neutral"; }
                                                
                                                        my $sum = itemsum($username, 1);
                                                        my $minroll = ($opts{disablestats} ? int($sum * 0.1) : int($sum * ($rps{$username}{stat_combat} / 50)));
                                                        my $maxroll = ($opts{disablestats} ? $sum : int($sum * (1 + $rps{$username}{stat_strength} / 50)));

                                                
                                                        cmdresponse("$rps{$username}{username} ($rps{$username}{nick}): Level $rps{$username}{level} ".
                                                                        "$rps{$username}{class}; Status: O".
                                                                        (isonline($username)?"n":"ff")."line; ".
                                                                        "Alignment: $align; ".
                                                                        "TTL: ".duration($rps{$username}{next})."; ".
                                                                        "Idled: ".duration($rps{$username}{idled})."; ".
                                                                        "Item sum: ".itemsum($username,0)." ($minroll - $maxroll); ".
                                                                        "Actions: " .$rps{$username}{actionpoints}. "; ".
                                                                        ($opts{disablestats} ? '' : "Stats: $rps{$username}{stat_points} free, $rps{$username}{stat_combat} combat, $rps{$username}{stat_strength} strength, $rps{$username}{stat_endurance} endurance; ")
                                                                        ,$nick);
                                                }
                                        }
                                }
                        }
                        elsif ($command eq "whoami" || $command eq "w")
                        {
                                if (!defined($username)) 
                                {
                                        cmdresponse("You are not logged in.", $nick);
                                }
                                else 
                                {
                                        
                                        if ($rps{$username}{level} > $opts{duelminimumlevel})
                                        {
                                                cmdresponse("You are $rps{$username}{username}, the level ".
                                                                $rps{$username}{level}." $rps{$username}{class}. ".
                                                                "Next level in ".duration($rps{$username}{next}) . ". ".
                                                                "You have $rps{$username}{actionpoints} action points.",
                                                                $nick);
                                        }
                                        else
                                        {
                                                cmdresponse("You are $rps{$username}{username}, the level ".
                                                                $rps{$username}{level}." $rps{$username}{class}. ".
                                                                "Next level in ".duration($rps{$username}{next}),
                                                                $nick);
                                        }
                                }
                        }
                        elsif ($command eq "top" || $command eq "t")
                        {
                                my @u = sort { $rps{$b}{level} <=> $rps{$a}{level} ||
                                           $rps{$a}{next}  <=> $rps{$b}{next} } grep { $_ ne $opts{owner} } keys(%rps);
                                            
                                
                                if (@u)
                                {
                                        my $response = "Idle RPG Top Players: ";  
                                        for my $i (0..2) 
                                        {
                                                $#u >= $i and
                                                $response .= "$rps{$u[$i]}{username}, the level $rps{$u[$i]}{level} ".
                                                                "$rps{$u[$i]}{class}, is #" . ($i + 1) . "!";
                                                $response .= " -- " if ($i < 2) ;
                                        }
                                        cmdresponse($response, $nick);
                                }
                        }
                        elsif ($command eq "finduser" || $command eq "f")
                        {
                                if ($msg =~ /^(?:finduser|f)\s+(.+)$/i)
                                {
                                        my $u = finduser(lc($1));
                                        if (!defined ($u))
                                        {
                                                cmdresponse("Unable to find an online user with the nickname $1", $nick);
                                        }
                                        else
                                        {
                                                cmdresponse("Username for $1's online user is $rps{$u}{username}", $nick);
                                        }
                                }
                                else
                                {
                                        cmdresponse("Try: FINDUSER <nick name>", $nick);
                                }
                        }
                        elsif ($command eq "seen")
                        {
                                if ($msg =~ /^(?:seen)\s+([^\:]+)$/i)
                                {
                                        my $u = lc($1);
                                        if (!exists($rps{$u}))
                                        {
                                                cmdresponse("No such user.",$nick);
                                        }
                                        else
                                        {
                                                if ($rps{$u}{lastactivity} && ha($username))
                                                {
                                                        cmdresponse("I have last seen $rps{$u}{username} ".duration(time() - $rps{$u}{lastactivity})." ago", $nick);
                                                }
                                                else
                                                {
                                                        cmdresponse("I have not seen $rps{$u}{username}", $nick);
                                                }
                                        }
                                }
                                else
                                {
                                        cmdresponse("Try: SEEN <username>", $nick);
                                }
                        
                                #lastactivity
                        }
                        elsif ($command eq "stats" || $command eq "s") 
                        {
                                if (!defined($username)) {
                                        cmdresponse("You are not logged in.", $nick);
                                }
                                else 
                                {
                                        if ($opts{disablestats})
                                        {
                                                cmdresponse("Stats are disabled", $nick);
                                                return;
                                        }
                                        
                                        cmdresponse("You have " . (($rps{$username}{stat_points} > 0) ? $rps{$username}{stat_points} : 'no') .
                                        " available point".(($rps{$username}{stat_points} == 1) ? '' : 's')." and the following stats: level $rps{$username}{stat_combat} combat,".
                                        " level $rps{$username}{stat_strength} strength and level $rps{$username}{stat_endurance}".
                                        " endurance. Use the command ADDSTAT to change these stats.",
                                        $nick);
                                
                                }
                        }
                        elsif ($command eq "resetstats" || $command eq "statreset")
                        {
                                if (!defined($username))
                                {
                                        cmdresponse("You are not logged in.", $nick);
                                        return;
                                }
                                
                                if ($opts{disablestats})
                                {
                                        cmdresponse("Stats are disabled", $nick);
                                        return;
                                }
                                
                                my $oldEndurance = $rps{$username}{stat_endurance};
                                
                                $rps{$username}{stat_combat} = 0;
                                $rps{$username}{stat_strength} = 0;
                                $rps{$username}{stat_endurance} = 0;
                                $rps{$username}{stat_points} = $rps{$username}{level} + 10;
                                cmdresponse("All stats have been reset.", $nick);
                                
                                if ($rps{$username}{stat_endurance} != $oldEndurance)
                                {
                                        #recalculate TTL
                                        my $normalNext = $rps{$username}{next} / ((100 - $oldEndurance) / 100); # The current TTL as if he had no endurance
                                        $rps{$username}{next} = ceil($normalNext * ((100 - $rps{$username}{stat_endurance}) / 100)); # New current TTL
                                        
                                        my $normalTTL = $rps{$username}{TTL} / ((100 - $oldEndurance) / 100); # The current TTL as if he had no endurance
                                        $rps{$username}{TTL} = ceil($normalTTL * ((100 - $rps{$username}{stat_endurance}) / 100)); # New current TTL
                                }
                                
                                penalize($username, "resetstats");
                                
                        }
                        elsif ($command eq "addstat" || $command eq "a") 
                        {
                                if (!defined($username))
                                {
                                        cmdresponse("You are not logged in.", $nick);
                                        return;
                                }
                
                                if ($opts{disablestats})
                                {
                                        cmdresponse("Stats are disabled", $nick);
                                        return;
                                }
        
                                if (lc($msg) =~ /^(?:addstat|a)\s+(c|combat|s|strength|e|endurance)(?:\s+(\-?\d+))?\s*$/i) 
                                {
                                        my $stat = $1;
                                        if ($stat eq "c") { $stat = "combat"; }
                                        elsif ($stat eq "s") { $stat = "strength"; }
                                        elsif ($stat eq "e") { $stat = "endurance"; }
                                        
                                        my $stat_stat = "stat_$stat";
                                        my $addpoints = ((!defined($2) || $2 eq '') ? 1 : int($2));
                                        my $oldEndurance = $rps{$username}{stat_endurance}; # needed to recalculate the endurance
                                        
                                        if (!$addpoints)
                                        {
                                                cmdresponse("You can not add 0 stat points");
                                                return;
                                        }
                                        
                                        if ($addpoints < 0) # Negative stats ARE supported if this line is removed
                                        {
                                                cmdresponse("You must use the RESETSTATS command to lower stats. This command will incur a small penalty.", $nick);
                                                return;
                                        }
                                        
                                        if ($addpoints > 0 && $rps{$username}{stat_points} <= 0)
                                        {
                                                cmdresponse("You do not have any stat points to spend", $nick);
                                                return;
                                        }
                                        
                                        
                                        if ($addpoints > $rps{$username}{stat_points}) 
                                        {
                                                $addpoints = $rps{$username}{stat_points};
                                        }
                                        elsif (($addpoints < 0) && (-$addpoints > $rps{$username}{$stat_stat})) 
                                        {
                                                $addpoints = -$rps{$username}{$stat_stat};
                                                if (!$addpoints)
                                                {
                                                        cmdresponse("You can not lower the stat $stat under level 0.", $nick);
                                                }
                                        }
                                        
                                        $rps{$username}{$stat_stat} += $addpoints;
                                        $rps{$username}{stat_points} -= $addpoints;
                                        
                                        if ($stat_stat eq "stat_endurance")
                                        {
                                                if ($rps{$username}{$stat_stat} > 50)
                                                {
                                                        my $diff = $rps{$username}{$stat_stat} - 50;
        
                                                        if ($diff)
                                                        {
                                                                $addpoints -= $diff;
                                                                $rps{$username}{$stat_stat} -= $diff;
                                                                $rps{$username}{stat_points} += $diff;
                                                                cmdresponse("You can not raise the stat $stat over level 50. $addpoints point".(($addpoints == 1) ? '' : 's')." added to " . $stat, $nick);
                                                        }
                                                        else
                                                        {
                                                                cmdresponse("You can not raise the stat $stat over level 50.", $nick);
                                                        }
                                                }
                                                else 
                                                {
                                                        if ($addpoints > 0) 
                                                        {
                                                            cmdresponse("$addpoints point".(($addpoints == 1) ? '' : 's')." added to " . $stat, $nick);
                                                        }
                                                        elsif ($addpoints < 0) 
                                                        {
                                                            cmdresponse(-$addpoints ." point".(($addpoints == 1) ? '' : 's')." removed from " . $stat, $nick);
                                                        }
                                                }
                                                
                                                # Last resort check, if this happends player may lose stat points, 
                                                # but more serious bugs can happen without this check.
                                                $rps{$username}{$stat_stat} = 0 if ($rps{$username}{$stat_stat} < 0);
                                                $rps{$username}{$stat_stat} = 50 if ($rps{$username}{$stat_stat} > 50);
                                        }
                                        else
                                        {
                                                if ($addpoints > 0) 
                                                {
                                                    cmdresponse("$addpoints point".(($addpoints == 1) ? '' : 's')." added to " . $stat, $nick);
                                                }
                                                elsif ($addpoints < 0) 
                                                {
                                                    cmdresponse(-$addpoints ." point".(($addpoints == 1) ? '' : 's')." removed from " . $stat, $nick);
                                                }
                                        }
                                        
                                        if ($rps{$username}{stat_endurance} != $oldEndurance)
                                        {
                                                #recalculate TTL
                                                my $normalNext = $rps{$username}{next} / ((100 - $oldEndurance) / 100); # The current TTL as if he had no endurance
                                                $rps{$username}{next} = ceil($normalNext * ((100 - $rps{$username}{stat_endurance}) / 100)); # New current TTL
                                                
                                                my $normalTTL = $rps{$username}{TTL} / ((100 - $oldEndurance) / 100); # The current TTL as if he had no endurance
                                                $rps{$username}{TTL} = ceil($normalTTL * ((100 - $rps{$username}{stat_endurance}) / 100)); # New current TTL
                                        }
                                }
                                else 
                                {
                                        cmdresponse("Try: ADDSTAT <combat/strength/endurance> <points>", $nick);
                                }
                                
                        }
                        elsif ($command eq "fixallstatpoints" && ha($username))
                        {
                                my $matches = 0;
                                foreach (keys %rps) 
                                {
                                        my $u = $_; 
                                        if ($rps{$u}{stat_combat} + 
                                            $rps{$u}{stat_strength} + 
                                            $rps{$u}{stat_endurance} + 
                                            $rps{$u}{stat_points}
                                             
                                            != $rps{$u}{level} + 10)
                                        {
                                              $rps{$u}{stat_combat} = 0;
                                              $rps{$u}{stat_strength} = 0;
                                              $rps{$u}{stat_endurance} = 0;
                                              $rps{$u}{stat_points} = $rps{$u}{level} + 10;
                                              $matches++;
                                        }
                                }
                                cmdresponse("Done, $matches matches.", $nick);
                        }
                        elsif ($command eq "resetallstatpoints" && ha($username))
                        {
                                my $matches = 0;
                                foreach (keys %rps) 
                                {
                                        my $u = $_; 
                                        $rps{$u}{stat_combat} = 0;
                                        $rps{$u}{stat_strength} = 0;
                                        $rps{$u}{stat_endurance} = 0;
                                        $rps{$u}{stat_points} = $rps{$u}{level} + 10;
                                        $matches++;
                                        
                                }
                                cmdresponse("Done, $matches stats reset.", $nick);
                        }
                        elsif ($command eq "addaction" && ha($username))
                        {
                                if ($msg =~ /^addaction\s+([^\:]+)\s*:\s*(?:(\-?\d+))?\s*$/i)
                                {
                                        my $u = lc($1);
                                        if (!exists($rps{$u})) 
                                        {
                                                cmdresponse("No such username $1.", $nick);
                                        }
                                        else
                                        {
                                                my $addpoints = (($2 eq '') ? 1 : int($2));
                                               
                                                if ($addpoints > 0)
                                                {
                                                        addactionpoints($u, $addpoints);
                                                }
                                                else
                                                {
                                                        removeactionpoints($u, $addpoints);
                                                }
                                               
                                                cmdresponse("$addpoints action point".(($addpoints == 1) ? '' : 's'). " added to the available action points of $rps{$u}{username}", $nick);
                                        }
                                }
                                else 
                                {
                                        cmdresponse("Try: ADDACTION <char name>:<points>", $nick);
                                }
                        }
                        elsif ($command eq "addstatpoints" && ha($username))
                        {
                                if ($msg =~ /^addstatpoints\s+([^\:]+)\s*:\s*(?:(\-?\d+))?\s*$/i)
                                {
                                        my $u = lc($1);
                                        if (!exists($rps{$u})) 
                                        {
                                                cmdresponse("No such username $1.", $nick);
                                        }
                                        else 
                                        {
                                               my $addpoints = (($2 eq '') ? 1 : int($2));
                                               $rps{$u}{stat_points} += $addpoints;
                                               
                                               cmdresponse("$addpoints point".(($addpoints == 1) ? '' : 's'). "added to the available points of $rps{$u}{username}", $nick); 
                                        }
                                }
                                else {
                                        cmdresponse("Try: ADDSTATPOINTS <char name>:<points>", $nick);
                                }
                        }
                        elsif ($command eq "items" || $command eq "i") 
                        {
                                if ($msg =~ /^(?:items|i)(?:\s+([^\:]+)\s*)?$/i)
                                {
                                        if ($1 && !exists($rps{lc($1)}))
                                        {
                                                cmdresponse("No such user.",$nick);
                                        }
                                        else
                                        {
                                                my $u = $username;
                                                
                                                $u = lc($1) if ($1);
                                        
                                                if (!defined($u)) 
                                                {
                                                        cmdresponse("You are not logged in.", $nick);
                                                }
                                                else
                                                {
                                                        my $items = '';
                                                        
                                                        for my $item (@possible_items) 
                                                        {
                                                                my $itemvalue = $rps{$u}{item}{$item};
                                                                
                                                                
                                                                if ($itemvalue =~ /(\d+)([a-z])/)
                                                                {
                                                                        $itemvalue = $1;
                                                                        $items .= "lvl " . $itemvalue ." \u$special_items{$2}, ";
                                                                }
                                                                else
                                                                {
                                                                        $itemvalue =~ s/\D//g;
                                                                        $items .= "lvl " . $itemvalue ." \u$item, ";
                                                                }
                                                        }
                                                        
                                                        $items = substr($items, 0, -2); # remove the last ", "
                                                        
                                                        if ($u eq $username) 
                                                        {       
                                                                cmdresponse("You own the following items: $items.", $nick);
                                                        }
                                                        else
                                                        {
                                                                cmdresponse("$rps{$u}{username} owns the following items: $items.", $nick);
                                                        }
                                                }
                                        }
                                }
                        }
                        elsif ($command eq "newpass") 
                        {
                                # NEWPASS <old password> <new password>
                                if (!defined($username)) 
                                {
                                        cmdresponse("You are not logged in.", $nick)
                                }
#                                 elsif (!$rps{$username}{joinedarena})
#                                 {
#                                         cmdresponse("You must be in the same arena to change your password.", $nick)
#                                 }
                                elsif ($msg =~ /^newpass\s+([^\:]+)\s*:\s*([^\:]+)\s*$/i) 
                                {
                                        if ($rps{$username}{pass} eq crypt($1,$rps{$username}{pass})) 
                                        {
                                                $rps{$username}{pass} = crypt(trim($2),mksalt());
                                                cmdresponse("Your password was changed.",$nick);
                                        }
                                        else 
                                        {
                                                cmdresponse("Wrong old password",$nick);
                                        }
                                }
                                else 
                                {
                                        cmdresponse("Try: NEWPASS <old password>:<new password>", $nick);
                                }
                        }
                        elsif ($command eq "recover") 
                        {
                                if ($msg =~ /^recover\s+([^\:]+)\s*:\s*([^\:]+)\s*$/i) 
                                {
                                        my $u = lc($1);
                                        if (!exists $rps{$u}) 
                                        {
                                                cmdresponse("Sorry, no such account name.", $nick);
                                        }
                                        elsif (defined $username) 
                                        {
                                                cmdresponse("Sorry, you are already online as $username.", $nick);
                                        }
#                                       elsif (!$onarena{$nicklc}) 
#                                       {
#                                               cmdresponse("You must be in the same arena to use RECOVER", $nick);
#                                       }
                                        else
                                        {
                                                if ($rps{$u}{pass} eq crypt($2,$rps{$u}{pass})) 
                                                {
                                                        $rps{$u}{nick} = $nicklc;
                                                        cmdresponse("You are now the owner of $rps{$u}{username}",$nick);
                                                }
                                                else 
                                                {
                                                        cmdresponse("Wrong password",$nick);
                                                }
                                        }
                                }
                                else
                                {
                                        cmdresponse("Try: RECOVER <char name>:<recover password>", $nick);
                                }
                        }
                        elsif ($command eq "chnick" && ha($username))
                        {
                                if ($msg =~ /^chnick\s+([^\:]+)\s*:\s*(.+)\s*$/i) 
                                {
                                        my $u = lc($1);
                                        my $nicklc = lc($2);

                                        if (!exists $rps{$u})
                                        {
                                                cmdresponse("Sorry, no such account name.", $nick);
                                        }
                                        else
                                        {
                                                cmdresponse("Changed the nickname from '".$rps{$u}{nick}."' to '".$nicklc."'.", $nick);
                                                $rps{$u}{nick} = $nicklc;
                                        }
                                }
                                else
                                {
                                        cmdresponse("Try: CHNICK <char name>:<nickname>", $nick);
                                } 
                        }
                        elsif ($command eq "sex")
                        {
                                cmdresponse("Unf! Unf! Unf!", $nick);
                        }
                        elsif ($command eq "sheep")
                        {
                                cmdresponse("Baa! Baa..... afk", $nick);
                        }
                        elsif ($command eq "align") 
                        {
                                if (!defined($username)) {
                                        cmdresponse("You are not logged in.", $nick)
                                }
                                # make sure the $msg is lowercase
                                elsif (lc($msg) =~ /^align\s+(good|evil|neutral)\s*$/) 
                                {       
                                        my $newalign = substr($1,0,1);
                                        if ($rps{$username}{alignment} eq $newalign)
                                        {
                                                cmdresponse("You are already $1.", $nick);
                                        }
                                        else
                                        {
                                                $rps{$username}{alignment} = $newalign; #first letter
                                                cmdresponse("Your alignment was changed to $1.", $nick);
                                                #chatmsg("$rps{$username}{username} has changed alignment to: $1.");
                                        }
                                }
                                else 
                                {
                                        cmdresponse("Try: ALIGN <good|neutral|evil>", $nick);
                                }
                        }
                        elsif ($command eq "gender")
                        {
                                if (!defined($username)) 
                                {
                                        cmdresponse("You are not logged in.", $nick)
                                }
                                # make sure the $msg is lowercase
                                elsif (lc($msg) =~ /^gender\s+(male|female)\s*$/) 
                                {       
                                        my $newgender = substr($1,0,1);
                                        if ($rps{$username}{gender} eq $newgender)
                                        {
                                                cmdresponse("You are already $1.", $nick);
                                        }
                                        else
                                        {
                                                $rps{$username}{gender} = $newgender; #first letter
                                                cmdresponse("Your gender was changed to $1.", $nick);
                                                #chatmsg("$rps{$username}{username} has changed alignment to: $1.");
                                        }
                                }
                                else 
                                {
                                        cmdresponse("Try: GENDER <male|female>", $nick);
                                }
                        }
                        elsif ($command eq "removeme") 
                        {
                                if (!defined($username)) 
                                {
                                        cmdresponse("You are not logged in.", $nick)
                                }
#                                 elsif (!$rps{$username}{joinedarena})
#                                 {
#                                         cmdresponse("You must be in the same arena to use REMOVEME.", $nick)
#                                 }
                                elsif ($msg =~ /^removeme\s+([^\:]+)\s*$/i) 
                                {
                                        if ($rps{$username}{pass} eq crypt($1,$rps{$username}{pass}))
                                        {
                                                cmdresponse("Account $rps{$username}{username} removed.",$nick);
                                                chatmsg("$nick removed ".g($username, "his", "her")." account, $rps{$username}{username}, the ". $rps{$username}{class}.".");
                                                delete($rps{$username});
                                        }
                                        else
                                        {
                                                cmdresponse("Wrong password");
                                        }
                                }
                                else {
                                    cmdresponse("Try: REMOVEME <password>");
                                }
                        }
                        elsif ($command eq "help") 
                        {
                                if (!ha($username))
                                {
                                        cmdresponse("For information on IRPG bot commands, see ".$opts{helpurl} . " or try CMDLIST", $nick);
                                }
                                else 
                                {
                                        cmdresponse("Help URL is $opts{helpurl}", $nick);
                                        cmdresponse("Admin commands URL is $opts{admincommurl}", $nick);
                                }
                        }
                        elsif ($command eq "die" && ha($username))
                        {
                                if ($msg =~ /^die\s+(.+)$/i) 
                                {
                                        chatmsg("DIE from $nick, reason: ($1)",1);
                                }
                                else
                                {
                                        chatmsg("DIE from $nick\r\n",1);
                                }
                                $opts{reconnect} = 0;

                                writedb();
                                close($sock);
                                print ("die from $nick");
                                exit(0);
                        }
                        elsif (($command eq "reloaddb") && ha($username)) 
                        {
                                if (!$pausemode) 
                                {
                                        cmdresponse("ERROR: Can only use LOADDB while in PAUSE mode.", $nick);
                                }
                                else 
                                {
                                        loaddb();
                                        readignorelist();
                                        cmdresponse("Reread player database file; ".scalar(keys(%rps)).
                                                        " accounts loaded.",$nick);
                                }
                        }
                        elsif(($command eq "readignorelist") && ha($username))
                        {
                                readignorelist();
                                cmdresponse("Reread ignore list; ".scalar(keys(%ignore)) . " nicknames ignored.", $nick);
                        }
                        elsif (($command eq "backup") && ha($username)) 
                        {
                                writedb();
                                backup();
                                cmdresponse("$opts{dbfile} copied to dbbackup/$opts{dbfile}".time(),$nick);
                        }
                        elsif (($command eq "hog") && ha($username)) {
                                hog();
                        }
                        elsif (($command eq "godsend") && ha($username)) {
                                godsend();
                        }
                        elsif (($command eq "calamity") && ha($username)) {
                                calamity();
                        }
                        elsif (($command eq "goodness") && ha($username)) {
                                goodness();
                        }
                        elsif (($command eq "evilness") && ha($username)) {
                                evilness();
                        }
                        elsif (($command eq "pause") && ha($username) ){
                                writedb();
                                $pausemode = $pausemode ? 0 : 1;
                                cmdresponse("PAUSE_MODE set to $pausemode.",$nick);
                        }
                        elsif (($command eq "silent") && ha($username)) {
                                if ($msg =~ /^silent\s+([0123])$/i) 
                                {
                                        $silentmode = $1;
                                        cmdresponse("SILENT_MODE set to $silentmode.",$nick);
                                }
                                else 
                                {
                                        if ($silentmode < 3)
                                        {
                                                cmdresponse("Try: SILENT <mode>, where mode is 0 (normal), 1 (no chat), 2 (no priv) or 3 (no priv for admin)", $nick);
                                        }
                                }
                        }
                        elsif (($command eq "jump") && ha($username)) 
                        {
                                if ($msg =~ /^jump\s+([a-z0-9\.\-])\s*:\s*(\d+)$/i) {
                                        writedb();
                                        chatmsg("JUMP to $1:$2 from $nick");
                                        unshift(@{$opts{servers}},"$1:$2");
                                        close($sock);
                                        sleep(3);
                                        goto CONNECT;
                                        return;
                                }
                                else {
                                        cmdresponse("Try: JUMP <server>:<port>", $nick);
                                }
                        }
                        elsif (($command eq "reconnect") && (ha($username) || $nicklc eq 'rareitanium')) 
                        {
                                if ($msg =~ /^reconnect\s+(.+)$/i) {
                                        chatmsg("RECONNECT from $nick, reason: ($1)\r\n",1);
                                }
                                else {
                                        chatmsg("RECONNECT from $nick\r\n",1);
                                }
                                writedb();
                                close($sock);
                                sleep(3);
                                goto CONNECT;
                                return;
                        }
                        elsif (($command eq "restart") && ha($username)) {
                                if ($msg =~ /^restart\s+(.+)$/i) 
                                {
                                        chatmsg("RESTART from $nick, reason: ($1)\r\n",1);
                                }
                                else 
                                {
                                        chatmsg("RESTART from $nick\r\n",1);
                                }
                                $opts{reconnect} = 0;
                                writedb();
                                close($sock);
                                #exec("perl \"$0\"");
                                #die ("RESTART from $nick failed!");
                                print "Restarting...";
                                exit(1);
                        }
# TO EVIL                      
#                         elsif (($command eq "logoutall") && ha($username)) {
#                                 $rps{$_}{loggedin} = 0 for keys(%rps);
#                                 chatmsg("$nick has logged everybody out",1);
#                         }
                        elsif (($command eq "go") && ha($username)) {
                                if ($msg =~ /^go\s+(\S+)$/i) 
                                {
                                        chatmsg("Moving to new arena $1 by $nick.",1);
                                        $opts{serverarena} = $1;
                                        sts("GO:$1");
                                }
                                else 
                                {
                                    cmdresponse("Try: GO <arena>", $nick);
                                }
                        }
                        elsif (($command eq "clearq") && ha($username)) 
                        {
                                undef(@queue);
                                chatmsg("Outgoing message queue cleared by $nick.");
                                cmdresponse("Outgoing message queue cleared.",$nick);
                        }
                        elsif (($command eq "onchat") && ha($username))
                        {
                                cmdresponse("Users found on chat: ". join(',', keys %onchat), $nick);
                        }
                        elsif (($command eq "money") && ha($username)) # hyperspace specific
                        {
                                sts("SEND:PRIV:$nick:?showmoney");
                        }
                        elsif (($command eq "giveme") && ha($username)) # hyperspace specific
                        {
                                if ($msg =~ /^giveme\s+(\d+)$/i)
                                {
                                        sts("SEND:PRIV:$nick:?give $1 Here you go");
                                }
                                else
                                {
                                        cmdresponse("Try: GIVEME <money>", $nick);
                                }
                        }
                        elsif ($command eq "pop" || $command eq "population")  # hyperspace specific
                        {
                                my $nick_;
                                my $totalPop = 0;
                                my $totalNonBots = 0;
                                my $totalPlaying = 0;
                                foreach $nick_ (keys %onarena)
                                {
                                        next unless ($onarena{$nick_});
                                        $totalPop++;
                                        $totalPlaying++ unless ($onarena{$nick_} == SPECTATOR || $nick_ =~ /^(<)/);
                                        $totalNonBots++ unless ($nick_ =~ /^(ub-|\^|<)/i);
                                }
                                
                                cmdresponse("Total population: $totalNonBots (With bots: $totalPop). Playing: $totalPlaying", $nick);
                        }
                        #if ($msg =~ /^go\s+(\S+)$/i)
                        elsif ($command eq "info")
                        {
                                my $info;
                                if (!ha($username) && $opts{allowuserinfo}) {
                                        $info = "On via server: ".
                                                $opts{servers}->[0].
                                                ". Users online: ".scalar(grep { isonline($_) } keys(%rps)).
                                                ". Admins online: ".
                                                join(", ", map { $rps{$_}{nick} }
                                                #is the user admin, online, and really in the arena?
                                                grep { $rps{$_}{isadmin} && isonline($_) && $onarena{$rps{$_}{nick}}} keys(%rps)).".";
                                                
                                        cmdresponse($info, $nick);
                                }
                                elsif (ha($username)) 
                                {
                                        my $queuedbytes = 0;
                                        $queuedbytes += (length($_)+2) for @queue; # +2 = \r\n
                                        $info = sprintf(
                                                "%.2fKiB sent, %.2fKiB received in %s. %d IRPG users ".
                                                "online of %d total users. %d accounts created since ".
                                                "startup. PAUSE_MODE is %d, SILENT_MODE is %d. ".
                                                "Outgoing queue is %d bytes in %d items. On via: %s. ".
                                                "Admins online: %s.",
                                                $outbytes/1024,
                                                $inbytes/1024,
                                                duration(time()-$^T),
                                                scalar(grep { isonline($_) } keys(%rps)),
                                                scalar(keys(%rps)),
                                                $registrations,
                                                $pausemode,
                                                $silentmode,
                                                $queuedbytes,
                                                scalar(@queue),
                                                $opts{servers}->[0],
                                                join(", ",map { $rps{$_}{nick} }
                                                                  grep { $rps{$_}{isadmin} && isonline($_) && $onarena{$rps{$_}{nick}}}
                                                                  keys(%rps)));
                                        cmdresponse($info, $nick);
                                }
                        }
                        elsif ($command eq "login") 
                        {
                                # LOGIN <username>
                                if (defined($username)) 
                                {
                                        cmdresponse("Sorry, you are already online as $username.", $nick);
                                }
                                elsif ($msg =~ /^login\s+([^\:]+)$/i) 
                                {
                                        my $u = lc($1);
                                        
                                        
                                        if (!exists $rps{$u})
                                        {
                                                cmdresponse("Sorry, no such account name.", $nick);
                                        }
                                        elsif ($rps{$u}{loggedin})
                                        {
                                                cmdresponse("You are already logged in. If you are playing by ?chat only, it may take up to 30 seconds to be seen as online.", $nick);
                                        }
                                        else
                                        {
                                                if ($nicklc eq $rps{$u}{nick}) 
                                                {
                                                        $rps{$u}{loggedin} = 1;
                                                        $rps{$u}{lastseen} = time();
                                                        $rps{$u}{joinedarena} = time() if ($onarena{$rps{$u}{nick}});
                                                        
                                                        chatmsg("$rps{$u}{username}, the level $rps{$u}{level} $rps{$u}{class}, is now online from nickname $nick. Next level in ".
                                                                        duration($rps{$u}{next}).".");
                                                        cmdresponse("Logon successful. Next level in ".
                                                                   duration($rps{$u}{next}).". Make sure to join ?chat ".$opts{botchat}, $nick);
                                                }
                                                else 
                                                {
                                                        cmdresponse("You are not the owner of this account. Use RECOVER char name:password", $nick);
                                                }
                                        }
                                }
                                else {
                                        cmdresponse("Try: LOGIN <username>", $nick);
                                }
                        }
                        else
                        {
                                cmdresponse("No such command send HELP for help.", $nick) if ($subtype eq 'priv');
                        }
                }
                elsif ($subtype eq 'pub' || $subtype eq 'pubm') 
                { # MSG:PUB:name:msg
                        ($nick, $msg) = split(':', $remaining, 2);
                        my $nicklc = lc($nick);
                        return if ($ignore{$nicklc});
                        $username = finduser($nicklc);
                        
                        if (defined($username))
                        {
                                $rps{$username}{lastactivity} = time();
                        }
                        
                        arenalog('  '.prependSpaces($nick, LOGNICKLENGTH)."> $msg");
                }
                elsif ($subtype eq 'freq')
                {
                        ($nick, $msg) = split(':', $remaining, 2);
                        my $nicklc = lc($nick);
                        return if ($ignore{$nicklc});
                        $username = finduser($nicklc);
                        
                        if (defined($username))
                        {
                                $rps{$username}{lastactivity} = time();
                        }
                        arenalog('T '.prependSpaces($nick, LOGNICKLENGTH)."> $msg");
                }
                elsif ($subtype eq 'sysop')
                {
                        ($nick, $msg) = split(':', $remaining, 2);
                        arenalog('S '.$msg);
                }
                elsif ($subtype eq 'chat') 
                { # MSG:CHAT:channelnum:name> msg
                        ($chnum, $remaining) = split(':', $remaining, 2);
                        ($nick, $msg) = split('> ', $remaining, 2);
                        
                        my $nicklc = lc($nick);
                        return if ($ignore{$nicklc});
                        
                        $username = finduser($nicklc);
                        if (defined($username))
                        {
                                $rps{$username}{lastactivity} = time();
                        }
                        
                        $msg = trim($msg);
                
                        if ($chnum eq '1') 
                        {
                                 penalize(finduser($nicklc),"privmsg",length($msg));
                        }
                        
                        chatlog(gamelog("C $chnum:".prependSpaces($nick, LOGNICKLENGTH)."> $msg"));
                }
        }
}

# send ?chat and set up to parse the response
sub sendchatcmd 
{
        sts("SEND:CMD:?chat"); # Send ?chat command, 
        sts("SEND:PRIV:$primnick:NOOP"); # Immediately after that send a PRIV to yourself to end the ?chat response
        $chatCommandSent = time();
        $chatCommandReplyLines = 0;
        undef @onchat_buffer;
}

# Prepends spaces until the text becomes length
# It does not remove characters, so the text may be longer
sub prependSpaces
{
        my ($text, $length) = @_;
        
        my $spaces = $length - length($text);
        
        if ($spaces > 0)
        {
                return (' ' x $spaces) . $text;
        }
        else
        {
                return $text;
        }
}

sub sts 
{ # send to server
        my($text,$skipq) = @_;
        if ($skipq) 
        {
                if ($sock) 
                {
                        print $sock "$text\r\n";
                        $lastmessage_sent = time();
                        $outbytes += length($text) + 2;
                        debug("-> $text");
                }
                else 
                {
                        # something is wrong. the socket is closed. clear the queue
                        undef(@queue);
                        debug("\$sock isn't writeable in sts(), cleared outgoing queue.\n");
                        return;
                }
        }
        else
        {
                if ($freemessages > 0)
                {
                        --$freemessages;
                        print $sock "$text\r\n";
                        $lastmessage_sent = time();
                        $outbytes += length($text) + 2;
                }
                else
                {
                        push(@queue,$text);
                        #debug(sprintf("(q%03d) = %s\n",$#queue,$text));
                        if ($opts{maxqueue} && @queue > $opts{maxqueue})
                        {
                                undef(@queue);
                                chatmsg("Outgoing message queue has been cleared because it contained to many items.", 1); #use skipq to avoid recursion loop
                        }
                }
        }
}

sub fq 
{ # deliver message(s) from queue
        if (!@queue) 
        {
                ++$freemessages if ($freemessages < ($opts{canspam} ? 50 : 4));
                return undef;
        }
        
        my $sentbytes = 0;
        for (0..$freemessages)
        {
                last() if !@queue; # no messages left to send
                # lower number of "free" messages we have left
                my $line = shift(@queue);
                
                if ($sock)
                {
                        debug("(fm$freemessages) -> $line");
                        --$freemessages if $freemessages > 0;
                        print $sock "$line\r\n";
                        $lastmessage_sent = time();
                        $sentbytes += length($line) + 2;
                }
                else 
                {
                        undef(@queue);
                        debug("Disconnected: cleared outgoing message queue.");
                        last();
                }
                $outbytes += length($line) + 2;
        }
}

sub duration { # return human duration of seconds
        my $s = abs($_[0]);
        return "NA ($s)" if $s !~ /^\d+$/;
        $s = 0 if ($s < 0);
        return sprintf("%d day%s, %02d:%02d:%02d",$s/86400,int($s/86400)==1?"":"s",
                                   ($s%86400)/3600,($s%3600)/60,($s%60));
}

sub ts { # timestamp
        my @ts = localtime(time());
        return sprintf("[%02d/%02d/%02d %02d:%02d:%02d] ",
                                   $ts[3],$ts[4]+1,$ts[5]%100,$ts[2],$ts[1],$ts[0]);
}

sub hog { # summon the hand of god
        my @players = grep { isonline($_) } keys(%rps);
        my $player = $players[rand(@players)];
        my $win = int(rand(5));
        my $time = int(((5 + int(rand(71)))/100) * $rps{$player}{TTL} * 0.2);
        if ($win) {
                $time = updateTTL($player, -$time);
                chatmsg(clog("Verily I say unto thee, the Heavens have burst forth, ".
                                         "and the blessed hand of God carried $rps{$player}{username} ".
                                         duration($time)." toward level ".($rps{$player}{level}+1).
                                         "."));
        }
        else 
        {
            $time = updateTTL($player, $time);
                chatmsg(clog("Thereupon she stretched out her little finger among them ".
                                         "and consumed $rps{$player}{username} with fire, slowing the heathen ".
                                         duration($time)." from level ".($rps{$player}{level}+1).
                                         "."));
        }
        
        chatmsg("$rps{$player}{username} reaches next level in ".duration($rps{$player}{next}).".");
}

sub updateTTL 
{
        # positive is penalty
        # negative is good
        my $player = $_[0];
        my $time = $_[1];

        return $time unless exists($rps{$player}); 
        return 0 if $rps{$player}{next} <= 0;
        
        # dont penalize more then the ttl for this level
        $time = $rps{$player}{TTL} if $time > $rps{$player}{TTL};  
        $rps{$player}{next} = int($rps{$player}{next} + $time);
        return $time;
}

sub rpcheck { # check levels, update database
        # check splits hash to see if any split users have expired
        # send out $freemessages lines of text from the outgoing message queue
        fq();
 
        if (((time() - $lastmessage_sent) > 20) && ($lastmessage_sent > 0)) 
        {
                $lastmessage_sent = time(); #make sure no flood is caused if something goes wrong in sts()
                sts("NOOP", 1);
        }
        
        if (((time() - $lastmessage_received) > 15) && ($lastmessage_received_noop == 0) && ($lastmessage_received > 0)) 
        {
            $lastmessage_received_noop = 1;
            sts("SEND:PRIV:$primnick:NOOP");
        }
        
        if (((time() - $lastmessage_received) > 30) && ($lastmessage_received > 0)) 
        {
                # connection must have died...
                debug('Connection timed out...');
                writedb();
                close($sock);
                sleep(3);
                goto CONNECT;
                return;
        }
        
        if (!$chatCommandLastReply || time() - $chatCommandLastReply > 60) # Last reply to ?chat was over 1 minute ago
        {
                if ($chatCommandSent) # a ?chat command has already been sent
                {
                        if (time() - $chatCommandSent > 30)
                        {
                                $chatCommandSent = 0;
                                $chatCommandReplyLines = 0;
                                undef @onchat_buffer;
                                sendchatcmd();
                        }
                }
                else
                {
                        sendchatcmd();
                }
        }
        
        
        # clear registration limiting
        $lastreg = 0;
        my $online = scalar(grep { isonline($_) } keys(%rps));
        # there's really nothing to do here if there are no online users
        unless ($online)
        {
                $lasttime = time();
                return;
        }
        
        my $onlineevil = scalar(grep { isonline($_) && $rps{$_}{alignment} eq "e" } keys(%rps));
        my $onlinegood = scalar(grep { isonline($_) && $rps{$_}{alignment} eq "g" } keys(%rps));
        if (!$opts{noscale}) 
        {
                if (rand((20*86400)/$opts{self_clock}) < $online) { hog(); }
                if (rand((24*86400)/$opts{self_clock}) < $online) { team_battle(); }
                if (rand((8*86400)/$opts{self_clock}) < $online) { calamity(); }
                if (rand((4*86400)/$opts{self_clock}) < $online) { godsend(); }
                if (rand((8*86400)/$opts{self_clock}) < $onlineevil) { evilness(); }
                if (rand((12*86400)/$opts{self_clock}) < $onlinegood) { goodness(); }
        }
        else 
        {
#                 hog() if rand(4000) < 1;
#                 team_battle() if rand(1500) < 1;
#                 calamity() if rand(4000) < 1;
#                 godsend() if rand(2000) < 1;
#                 evilness() if (rand(6000) < 1);
#                 goodness() if (rand(6000) < 1);
                
                hog() if rand(4000) < $online/5;
                team_battle() if rand(1500) < $online/5;
                calamity() if rand(4000) < $online/5;
                godsend() if rand(2000) < $online/5;
                evilness() if (rand(6000) < $online/5);
                goodness() if (rand(6000) < $online/5);
        }
        

        moveplayers();

        # statements using $rpreport do not bother with scaling by the clock because
        # $rpreport is adjusted by the number of seconds since last rpcheck()
        if ($rpreport%120==0 && $opts{writequestfile}) { writequestfile(); }
        if (time() > $quest{qtime}) 
        {
                if (!@{$quest{questers}}) { quest(); }
                elsif ($quest{type} == 1) {
                        chatmsg(clog("$rps{$quest{questers}->[0]}{username}, $rps{$quest{questers}->[1]}{username}, $rps{$quest{questers}->[2]}{username} and ".
                                                        "$rps{$quest{questers}->[3]}{username} have blessed the realm by ".
                                                 "completing their quest! 25% of their burden is ".
                                                 "eliminated."));
                        for (@{$quest{questers}}) {
                                $rps{$_}{next} = int($rps{$_}{next} * .75);
                        }
                        undef(@{$quest{questers}});
                        $quest{qtime} = time() + 21600;
                }
                # quest type 2 awards are handled in moveplayers()
        }
        if ($rpreport && $rpreport%14340==0) # 4 hours - 1 minute
        {
                my @u = sort { $rps{$b}{level} <=> $rps{$a}{level} ||
                                           $rps{$a}{next}  <=> $rps{$b}{next} } grep { $_ ne $opts{owner} } keys(%rps);

                                           
                chatmsg("Idle RPG Top Players:") if @u;
                for my $i (0..2) {
                        $#u >= $i and
                        chatmsg("$rps{$u[$i]}{username}, the level $rps{$u[$i]}{level} ".
                                        "$rps{$u[$i]}{class}, is #" . ($i + 1) . "! Next level in ".
                                        (duration($rps{$u[$i]}{next})).".");
                }
                backup();
        }
        if ($rpreport%3600==0 && $rpreport)  # 1 hour
        { 
                my @players = grep { isonline($_) && $rps{$_}{level} > 10 } keys(%rps);
                
                # 20% of all players must be level 45+
                #if ((scalar(@players)/scalar(grep { isonline($_) } keys(%rps))) > .15)
                
                # there must be 3 level 10+ players online
                if (@players > 2)
                {
                        challenge_opp($players[int(rand(@players))]);
                }
        }
        elsif ($rpreport%250==0 && $rpreport) # 250 seconds (4 mins and 10 secs)
        { 
                my @players = grep { isonline($_) && $rps{$_}{level} > 10 } keys(%rps);
                                
                # there must be 5 level 10+ players online
                if (@players > 4)
                {
                        challenge_opp($players[int(rand(@players))]);
                }
        }
        
        if ($pausemode && $rpreport%300==0) 
        { # warn every 5m
                chatmsg("WARNING: Cannot write database in PAUSE mode!");
        }
        # do not write in pause mode, and do not write if not yet connected. (would
        # log everyone out if the bot failed to connect. $lasttime = time() on
        # successful join to $opts{botchan}, initial value is 1). if fails to open
        # $opts{dbfile}, will not update $lasttime and so should have correct values
        # on next rpcheck().
        if ($lasttime != 1)
        {
                my $curtime = time();
                my $timediff = ($curtime - $lasttime);
                $timediff = 0 if $timediff < 0;
                
                for my $k (keys(%rps)) 
                {
                        if (isonline($k))
                        {
                                #1 + (100-25) / 100
                                
                                $rps{$k}{next} -= $timediff;
                                $rps{$k}{idled} += $timediff;
                                $rps{$k}{nextbonusactionpoint} -= $timediff;
                                if ($rps{$k}{nextbonusactionpoint} < 1)
                                {
                                        $rps{$k}{nextbonusactionpoint} = $opts{actionextrapointtime};
                                        addactionpoints($k, 1);
                                }
                                
                                if ($rps{$k}{next} < 1)
                                {
                                        $rps{$k}{level}++;
                                        $rps{$k}{stat_points}++;
                                        addactionpoints($k, $opts{actionpointsperlevel});
                                        $rps{$k}{TTL} = getTTL($rps{$k}{level}) * ($opts{disablestats} ? 1 : ((100-$rps{$k}{stat_endurance}) / 100));
                                        $rps{$k}{TTL} = $opts{rpbase}  if ($rps{$k}{TTL} < $opts{rpbase}); 
                                        $rps{$k}{TTL} = int($rps{$k}{TTL});
                                        
                                        $rps{$k}{next} = $rps{$k}{TTL};
                                        chatmsg("$rps{$k}{username}, the $rps{$k}{class}, has attained level ".
                                                        "$rps{$k}{level}! Next level in ".
                                                        duration($rps{$k}{next}).".");
                                        find_item($k);
                                        
                                        if ($rps{$k}{level} < 20)
                                        {
                                                challenge_opp($k) if (rand(4) < 1);
                                        }
                                        else
                                        {
                                                challenge_opp($k);
                                        }
                                        
                                         
                                }
                        }


                }
                if (!$pausemode && $rpreport%60==0) { writedb(); }
                $rpreport += $opts{self_clock};
                $lasttime = $curtime;
        }
}

sub getTTL 
{ #without endurance stat
        my $level = $_[0];
        if ($level > $opts{rpttllinear}) 
        {
                my $linearbase = int($opts{rpbase} * ($opts{rpstep}**$opts{rpttllinear}));
                return $linearbase + ($level - $opts{rpttllinear}) * $opts{rpttllinearstep};
        }
        else 
        {
                return int($opts{rpbase} * ($opts{rpstep}**$level));
        }
}

sub getPen 
{
        my $level = $_[0];
        my $p = $_[1];
        if ($level > $opts{rpttllinear}) {
                my $linearbase = int($p * ($opts{rppenstep}**$opts{rpttllinear}));
                return $linearbase + ($level - $opts{rpttllinear}) * $opts{rpttllinearstep};
        }
        else {
                return int($p * ($opts{rppenstep}**$level));
        }
}

use constant BATTLE_TYPE_CHALLENGE => 0;
use constant BATTLE_TYPE_COLLISION => 1;
use constant BATTLE_TYPE_DUEL => 2;
use constant BATTLE_TYPE_SLAY => 3;

sub battle
{ # pit argument player against random player
        my ($u, $opp, $battle_type) = @_;
        
        $battle_type = BATTLE_TYPE_CHALLENGE unless($battle_type);
        
        return if ($battle_type == BATTLE_TYPE_SLAY && !defined($opp));
                
        
        unless (defined($opp))
        {
                my @opps = grep { isonline($_) && $u ne $_ } keys(%rps);
                push(@opps, $primnick);
                
                $opp = $opps[int(rand(@opps))];
        }
        
        my $u_username = $primnick; 
        $u_username = $rps{$u}{username} if (lc($u) ne lc($primnick));
        
        my $opp_username = $primnick;
        $opp_username = $rps{$opp}{username} if (lc($opp) ne lc($primnick) && $battle_type != BATTLE_TYPE_SLAY);
        
        my $mysum = itemsum($u,1);
        my $myminroll = ($opts{disablestats} || lc($u) eq lc($primnick)) ? int($mysum * 0.1) : int($mysum * ($rps{$u}{stat_combat} / 50));
        $mysum = int($mysum * (1 + $rps{$u}{stat_strength} / 50)) if (!$opts{disablestats} && lc($u) ne lc($primnick));
        $myminroll = $mysum if ($myminroll > $mysum);
        my $myroll = int(rand($mysum - $myminroll)) + $myminroll;
        
        my ($oppsum, $oppminroll, $opproll);
        
        if ($battle_type == BATTLE_TYPE_SLAY)
        {
                $oppsum = $monsters{$opp}{sum};
                $oppminroll = 0;
                $opproll = int(rand($oppsum));
        }
        else
        {
                $oppsum = itemsum($opp,1);
                $oppminroll = ($opts{disablestats} || lc($opp) eq lc($primnick)) ? int($oppsum * 0.1) : int($oppsum * ($rps{$opp}{stat_combat} / 50));
                $oppsum = int($oppsum * (1 + $rps{$opp}{stat_strength} / 50)) if (!$opts{disablestats} && lc($opp) ne lc($primnick));
                $oppminroll = $oppsum if ($oppminroll > $oppsum);
                $opproll = int(rand($oppsum - $oppminroll)) + $oppminroll;
        }
        
        if ($myroll >= $opproll)
        {
                if (lc($u) ne lc($primnick))
                {
                        my $gain = (lc($opp) eq lc($primnick)) ? 20 : $rps{$opp}{level}/4;
                        $gain = 7 if $gain < 7;
                        
                        $gain *= 0.3 if ($battle_type == BATTLE_TYPE_DUEL);
                        $gain *= rand() * 0.2 + 0.9;
                        
                        
                        $gain = int(($gain/100)*$rps{$u}{TTL}* 0.4);
                        
                        
                        
                        $gain = updateTTL($u, -$gain);
                
                        if ($battle_type == BATTLE_TYPE_CHALLENGE)
                        {
                                chatmsg(clog("$u_username [$myminroll/$myroll/$mysum] has challenged $opp_username [$oppminroll/$opproll/".
                                                         "$oppsum] in combat and won! ".duration($gain)." is ".
                                                         "removed from $u_username\'s clock. ".
                                                         "$u_username reaches next level in ".duration($rps{$u}{next})."."
                                                         ));
                        }
                        elsif ($battle_type == BATTLE_TYPE_COLLISION)
                        {
                                chatmsg(clog("$u_username [$myminroll/$myroll/$mysum] has come upon $opp_username [$oppminroll/$opproll/$oppsum".
                                                 "] and taken them in combat! ".duration($gain)." is ".
                                                 "removed from $rps{$u}{username}\'s clock. ".
                                                 "$u_username reaches next level in ".duration($rps{$u}{next})."."
                                                 ));
                        }
                        elsif ($battle_type == BATTLE_TYPE_DUEL)
                        {
                                chatmsg(clog("$u_username [$myminroll/$myroll/$mysum] has dueled $opp_username [$oppminroll/$opproll/$oppsum".
                                                 "] and took them in combat! ".duration($gain)." is ".
                                                 "removed from $rps{$u}{username}\'s clock. ".
                                                 "$u_username reaches next level in ".duration($rps{$u}{next})."."
                                                 ));
                        }

                }
                else
                {
                        if ($battle_type == BATTLE_TYPE_CHALLENGE)
                        {
                                chatmsg(clog("$u_username [$myminroll/$myroll/$mysum] has challenged $opp_username [$oppminroll/$opproll/$oppsum] in combat and won! "));
                        }
                        elsif ($battle_type == BATTLE_TYPE_COLLISION)
                        {
                                chatmsg(clog("$u_username [$myminroll/$myroll/$mysum] has come upon $opp_username [$oppminroll/$opproll/$oppsum] and taken them in combat! "));
                        }
                        elsif ($battle_type == BATTLE_TYPE_DUEL)
                        {
                                chatmsg(clog("$u_username [$myminroll/$myroll/$mysum] has dueled $opp_username [$oppminroll/$opproll/$oppsum] and won! "));
                        }
                }
                my $csfactor;
                
                if ($battle_type == BATTLE_TYPE_DUEL)
                {
                        $csfactor = 4;
                }
                else
                {
                        $csfactor = $rps{$u}{alignment} eq "g" ? 25 : $rps{$u}{alignment} eq "e" ? 10 : 17;
                }
                       
                if ($battle_type != BATTLE_TYPE_DUEL)
                {
                        if (rand($csfactor) < 1 && lc($opp) ne lc($primnick) && lc($u) ne lc($primnick)) 
                        {
                                my $lvl = $rps{$opp}{level} - $rps{$u}{level} + 20;
                                $lvl = 1 if ($lvl < 1); 
                                $lvl = 35 if ($lvl > 35);
                                $lvl /= 20;
                                
                                
                                my $gain = ((5 + int(rand(20)))/100) * $lvl * $rps{$opp}{TTL} * 0.2;
                                $gain *= rand() * 0.2 + 0.9;
                                $gain = updateTTL($opp, int($gain));
                                chatmsg(clog("$u_username has dealt $opp_username a Critical Strike! ".
                                                         duration($gain)." is added to $opp_username\'s clock. ".
                                                         "$opp_username reaches next level in ".duration($rps{$opp}{next})."."
                                                         ));
                                #chatmsg("$opp_username reaches next level in ".duration($rps{$opp}{next}).".");
                        }
                        #elsif ($battle_type != BATTLE_TYPE_DUEL && rand(20) < ($rps{$u}{alignment} eq "g" && $rps{$opp}{alignment} eq "e" ? 4 : 1) && lc($opp) ne lc($primnick) && $rps{$u}{level} > 19 && lc($opp) ne lc($opts{owner}))
                        elsif (rand(20) < ($rps{$u}{alignment} eq "g" && $rps{$opp}{alignment} eq "e" ? 4 : 1) && lc($opp) ne lc($primnick) && $rps{$u}{level} > 19)
                        {
                                #my @items = ("ring","amulet","charm","weapon","helm","tunic",
                                #                         "pair of gloves","set of leggings","shield",
                                #                         "pair of boots");
                                my $type = $possible_items[rand(@possible_items)];
                                if (lvl($rps{$opp}{item}{$type}) > lvl($rps{$u}{item}{$type})) {
                                        chatmsg(clog("In the fierce battle, $opp_username dropped ".g($opp_username, "his", "her")." level ".
                                                                 lvl($rps{$opp}{item}{$type})." $type! $u_username picks ".
                                                                 "it up, tossing ".g($u_username, "his", "her")." old level ".
                                                                 lvl($rps{$u}{item}{$type})." $type to $opp_username."));
                                        my $tempitem = $rps{$u}{item}{$type};
                                        $rps{$u}{item}{$type}=$rps{$opp}{item}{$type};
                                        $rps{$opp}{item}{$type} = $tempitem;
                                }
                        }
                }
        }
        else 
        {
                if (lc($u) ne lc($primnick))
                {
                        my $lvl = $rps{$u}{level} - $rps{$opp}{level} + 10;
                        $lvl = 5 if ($lvl < 5); 
                        $lvl = 50 if ($lvl > 50);
                        
                        my $gain = (lc($opp) eq lc($primnick)) ? 10 : $lvl/3;
                        $gain = 7 if $gain < 7;
                        $gain *= 0.3 if ($battle_type == BATTLE_TYPE_DUEL);
                        $gain *= rand() * 0.2 + 0.9;
                        $gain = int(($gain/100)*$rps{$u}{TTL} * 0.4);
                        $gain = updateTTL($u, $gain);
                        
                        if ($battle_type == BATTLE_TYPE_CHALLENGE)
                        {
                                chatmsg(clog("$u_username [$myminroll/$myroll/$mysum] has challenged $opp_username [$oppminroll/$opproll/".
                                                 "$oppsum] in combat and lost! ".duration($gain)." is ".
                                                 "added to $u_username\'s clock. ".
                                                 "$u_username reaches next level in ".duration($rps{$u}{next})."."
                                                 ));
                        }
                        elsif ($battle_type == BATTLE_TYPE_COLLISION)
                        {
                                chatmsg(clog("$u_username [$myminroll/$myroll/$mysum] has come upon $opp_username [$oppminroll/$opproll/".
                                                 "$oppsum] and been defeated in combat! ".duration($gain)." is ".
                                                 "added to $u_username\'s clock. ".
                                                 "$u_username reaches next level in ".duration($rps{$u}{next})."."
                                                 ));
                        }
                        elsif ($battle_type == BATTLE_TYPE_DUEL)
                        {
                                chatmsg(clog("$u_username [$myminroll/$myroll/$mysum] has dueled $opp_username [$oppminroll/$opproll/".
                                                 "$oppsum] and has been defeated in combat! ".duration($gain)." is ".
                                                 "added to $u_username\'s clock. ".
                                                 "$u_username reaches next level in ".duration($rps{$u}{next})."."
                                                 ));
                        }
                }
                else
                {
                        if ($battle_type == BATTLE_TYPE_CHALLENGE)
                        {
                                chatmsg(clog("$u_username [$myminroll/$myroll/$mysum] has challenged $opp_username [$oppminroll/$opproll/$oppsum] in combat and lost! "));
                        }
                        elsif ($battle_type == BATTLE_TYPE_COLLISION)
                        {
                                chatmsg(clog("$u_username [$myminroll/$myroll/$mysum] has come upon $opp_username [$oppminroll/$opproll/$oppsum] and been defeated in combat! "));
                        }
                        elsif ($battle_type == BATTLE_TYPE_DUEL)
                        {
                                chatmsg(clog("$u_username [$myminroll/$myroll/$mysum] has dueled $opp_username [$oppminroll/$opproll/$oppsum] and has been defeated in combat! "));
                        }
                }
        }
}

sub challenge_opp
{
        battle($_[0], $_[1], BATTLE_TYPE_CHALLENGE);
}

sub collision_fight
{
        battle($_[0], $_[1], BATTLE_TYPE_COLLISION);
}

sub duel
{
        removeactionpoints($_[0], 1);
        battle($_[0], $_[1], BATTLE_TYPE_DUEL);
}

sub slay_fight 
{
        battle($_[0], $_[1], BATTLE_TYPE_SLAY);
}

sub addactionpoints
{
        my ($u, $amount) = @_;
        $u = lc($u);
        
        $rps{$u}{actionpoints} += $amount;
        
        $rps{$u}{actionpoints} = $opts{actionmaxpoints}
                if ($rps{$u}{actionpoints} > $opts{actionmaxpoints});
}

sub removeactionpoints
{
        my ($u, $amount) = @_;
        $u = lc($u);
        
        $rps{$u}{actionpoints} -= $amount;
        $rps{$u}{actionpoints} = 0
                if ($rps{$u}{actionpoints} < 0);
                
}

sub team_battle { # pit three players against three other players
        my @opp = grep { isonline($_) } keys(%rps);
        return if @opp < 6;
        splice(@opp,int(rand(@opp)),1) while @opp > 6;
        fisher_yates_shuffle(\@opp);

        my $myminroll = 0;
        my $oppminroll = 0;
        my $mysum = 0;
        my $oppsum = 0;
        my $myroll = 0;
        my $opproll = 0;
        
        for ($a = 0; $a < 6; $a++)
        {
                my $sum = itemsum($opp[$a], 1);
                my $minroll = ($opts{disablestats} ? int($sum * 0.1) : int($sum * ($rps{$opp[$a]}{stat_combat} / 50)));
                $sum = int($sum * (1 + $rps{$opp[$a]}{stat_strength} / 50)) unless ($opts{disablestats});
                $minroll = $sum if ($minroll > $sum);
                my $roll = int(rand($sum - $minroll)) + $minroll;
                
                if ($a < 3)
                {
                        $myminroll += $minroll;
                        $mysum += $sum;
                        $myroll += $roll;
                }      
                else
                {
                        $oppminroll += $minroll;
                        $oppsum += $sum;
                        $opproll += $roll;
                }                                    
        }

        
        my $gain = $rps{$opp[0]}{TTL};
        for my $p (1,2) {
                $gain = $rps{$opp[$p]}{TTL} if $gain > $rps{$opp[$p]}{TTL};
        }
        $gain = int($gain*.20 * 0.2);
        if ($myroll >= $opproll) {
                chatmsg(clog("$rps{$opp[0]}{username}, $rps{$opp[1]}{username}, and $rps{$opp[2]}{username} [$myminroll/$myroll/$mysum] have ".
                                         "team battled $rps{$opp[3]}{username}, $rps{$opp[4]}{username}, and $rps{$opp[5]}{username} [$oppminroll/$opproll/".
                                         "$oppsum] and won! ".duration($gain)." is removed from ".
                                         "their clocks."));
                updateTTL($opp[0], -$gain);
                updateTTL($opp[1], -$gain);
                updateTTL($opp[2], -$gain);
        }
        else {
                chatmsg(clog("$rps{$opp[0]}{username}, $rps{$opp[1]}{username}, and $rps{$opp[2]}{username} [$myminroll/$myroll/$mysum] have ".
                                         "team battled $rps{$opp[3]}{username}, $rps{$opp[4]}{username}, and $rps{$opp[5]}{username} [$oppminroll/$opproll/".
                                         "$oppsum] and lost! ".duration($gain)." is added to ".
                                         "their clocks."));
                updateTTL($opp[0], $gain);
                updateTTL($opp[1], $gain);
                updateTTL($opp[2], $gain);
        }
}

sub find_item #sub item
{ # find item for argument player
        my ($u, $type, $silent) = @_;
        exists $rps{$u} or die;
        
        if (!$type)
        {
                $type = $possible_items[rand(@possible_items)];
        }
        else
        {
                my $found = 0;
                foreach (@possible_items)
                {
                        $found = 1 if ($_ eq $type);
                }
                
                if (!$found)
                {
                        $type = $possible_items[rand(@possible_items)];
                }
        }
        
        my $level = 1;
        my $ulevel;
        my $old_ulevel = lvl($rps{$u}{item}{$type});
        
        for my $num (1 .. int($rps{$u}{level}*1.5)) 
        {
                if (rand(1.4**($num/4)) < 1) 
                {
                        $level = $num;
                }
        }
        
        if ($rps{$u}{level} >= 25 && rand(40) < 1) 
        {
                $ulevel = 50 + int(rand(25));
                if ($ulevel >= $level && $ulevel > lvl($rps{$u}{item}{helm})) {
                        chatmsg("The light of the gods shines down upon $rps{$u}{username}! ".g($u, "He", "She")." has ".
                                   "found the level $ulevel $special_items{a}! ".
                                   "$rps{$u}{username}\'s enemies fall before you as ".g($u, "he", "she")." anticipates their ".
                                   "every move.") unless ($silent);
                                   
                        $rps{$u}{item}{helm} = $ulevel."a";
                        return;
                }
        }
        elsif ($rps{$u}{level} >= 25 && rand(40) < 1) {
                $ulevel = 50+int(rand(25));
                if ($ulevel >= $level && $ulevel > lvl($rps{$u}{item}{ring})) {
                        chatmsg("The light of the gods shines down upon $rps{$u}{username}! ".g($u, "He", "She")." has ".
                                   "found the level $ulevel $special_items{h}! ".
                                   "$rps{$u}{username}\'s enemies are blinded by both its glory ".
                                   "and their greed as she brings desolation upon them.") unless ($silent);
                        $rps{$u}{item}{ring} = $ulevel."h";
                        return;
                }
        }
        elsif ($rps{$u}{level} >= 30 && rand(40) < 1) {
                $ulevel = 75+int(rand(25));
                if ($ulevel >= $level && $ulevel > lvl($rps{$u}{item}{tunic})) {
                        chatmsg("The light of the gods shines down upon $rps{$u}{username}! ".g($u, "He", "She")." has ".
                                   "found the level $ulevel $special_items{b}! ".
                                   "$rps{$u}{username}\'s enemies cower in fear as their attacks have no ".
                                   "effect on him.") unless ($silent);
                        $rps{$u}{item}{tunic} = $ulevel."b";
                        return;
                }
        }
        elsif ($rps{$u}{level} >= 35 && rand(40) < 1) {
                $ulevel = 100+int(rand(25));
                if ($ulevel >= $level && $ulevel > lvl($rps{$u}{item}{amulet})) {
                        chatmsg("The light of the gods shines down upon $rps{$u}{username}! ".g($u, "He", "She")." has ".
                                   "found the level $ulevel $special_items{c}! ". 
                                   "$rps{$u}{username}\'s enemies are swept away by an elemental fury before the ".
                                   "war has even begun") unless ($silent);
                        $rps{$u}{item}{amulet} = $ulevel."c";
                        return;
                }
        }
        elsif ($rps{$u}{level} >= 35 && rand(40) < 1) {
                $ulevel = 150+int(rand(51));
                if ($ulevel >= $level && $ulevel > lvl($rps{$u}{item}{weapon})) {
                        chatmsg("The light of the gods shines down upon $rps{$u}{username}! ".g($u, "He", "She")." has ".
                                   "found the level $ulevel $special_items{j}! ".
                                   "$rps{$u}{username}\'s enemies quickly retreat as they try to cure".
                                   "their poisoned wounds.") unless ($silent);
                        $rps{$u}{item}{charm} = $ulevel."j";
                        return;
                }
        }
        elsif ($rps{$u}{level} >= 40 && rand(40) < 1) {
                $ulevel = 150+int(rand(25));
                if ($ulevel >= $level && $ulevel > lvl($rps{$u}{item}{weapon})) {
                        chatmsg("The light of the gods shines down upon $rps{$u}{username}! ".g($u, "He", "She")." has ".
                                   "found the level $ulevel $special_items{d}! ".
                                   "$rps{$u}{username}\'s enemies' hatred is brought to a quick end as ".g($u, "he", "she")." arcs his ".
                                   "wrist, dealing the crushing blow.") unless ($silent);
                        $rps{$u}{item}{weapon} = $ulevel."d";
                        return;
                }
        }
        elsif ($rps{$u}{level} >= 45 && rand(40) < 1) {
                $ulevel = 175+int(rand(26));
                if ($ulevel >= $level && $ulevel > lvl($rps{$u}{item}{weapon})) {
                        chatmsg("The light of the gods shines down upon $rps{$u}{username}! ".g($u, "He", "She")." has ".
                                   "found the level $ulevel $special_items{e}! ".
                                   "$rps{$u}{username}\'s enemies are tossed aside as ".g($u, "he", "she")." blindly swings his arm ".
                                   "around hitting stuff.") unless ($silent);
                        $rps{$u}{item}{weapon} = $ulevel."e";
                        return;
                }
        }
        elsif ($rps{$u}{level} >= 38 && rand(40) < 1) {
                $ulevel = 200+int(rand(101));
                if ($ulevel >= $level && $ulevel > lvl($rps{$u}{item}{"set of leggings"})) {
                        chatmsg("The light of the gods shines down upon $rps{$u}{username}! She has ".
                                   "found the level $ulevel $special_items{i}! ".
                                   "$rps{$u}{username}\'s enemies are stunned by the amazing beauty as they gaze upon her latest find."
                                   ) unless ($silent);
                        $rps{$u}{item}{"set of leggings"} = $ulevel."i";
                        return;
                }
        }
        elsif ($rps{$u}{level} >= 48 && rand(40) < 1) {
                $ulevel = 250+int(rand(51));
                if ($ulevel >= $level && $ulevel >
                        lvl($rps{$u}{item}{"pair of boots"})) {
                        chatmsg("The light of the gods shines down upon $rps{$u}{username}! ".g($u, "He", "She")." has ".
                                   "found the level $ulevel $special_items{f}! ".
                                   "$rps{$u}{username}\'s enemies are left choking on his dust as ".
                                    g($u, "he", "she")." runs from them very, very quickly.") unless ($silent);
                        $rps{$u}{item}{"pair of boots"} = $ulevel."f";
                        return;
                }
        }
        elsif ($rps{$u}{level} >= 52 && rand(40) < 1) {
                $ulevel = 300+int(rand(51));
                if ($ulevel >= $level && $ulevel > lvl($rps{$u}{item}{weapon})) {
                        chatmsg("The light of the gods shines down upon $rps{$u}{username}! ".g($u, "He", "She")." has ".
                                   "found the level $ulevel $special_items{g}! ".
                                   "$rps{$u}{username}\'s enemies are left with a sudden and intense clarity of ".
                                   "mind... even as ".g($u, "he", "she")." relieves them of it.") unless ($silent);
                        $rps{$u}{item}{weapon} = $ulevel."g";
                        return;
                }
        }
        elsif (rand(100) < 1) {
                $ulevel = 150+int(rand(101));
                if ($ulevel >= $level && $ulevel > lvl($rps{$u}{item}{ring})) {
                        chatmsg("The light of the gods shines down upon $rps{$u}{username}! ".g($u, "He", "She")." has ".
                                   "found the level $ulevel $special_items{m}! ".
                                   "$rps{$u}{username} scrambles to find a can of WD-40 before ".
                                   g($u, "his", "her")." most precious ring rusts away."
                                   ) unless ($silent);
                        $rps{$u}{item}{ring} = $ulevel."m";
                        return;
                }
        }
        
        if ($level > $old_ulevel)
        {
                privmsg("You found a level $level $type! Your current $type is only ".
                           "level ".$old_ulevel.", so it seems Luck is ".
                           "with you!",$rps{$u}{nick}) unless ($silent);
                $rps{$u}{item}{$type} = $level;
        }
        else 
        {
                my $oldlvl = $rps{$u}{item}{$type};
                $oldlvl =~ s/\D//g;
                privmsg("You found a level $level $type. Your current $type is level ".
                           "$oldlvl, so it seems Luck is against you. ".
                           "You toss the $type.",$rps{$u}{nick}) unless ($silent);
        }
}

sub readignorelist
{
        undef %ignore;
        return unless ($opts{ignorelist});
        
        unless (open(IGNORE, $opts{ignorelist}))
        {
                chatmsg("WARNING: readignorelist() failed: $!");
                return;
        }
        
        my ($line, $nick);
        while ($line = <IGNORE>)
        {
                chomp($line);
                next unless($line);
                next if $line =~ /^#/; # skip comments
                $nick = lc($line);
                $ignore{$nick} = 1;
                
                delete($onarena{$nick});
                delete($onchat{$nick});
        }
        close(IGNORE);
        debug("ignore list read: ".scalar(keys(%ignore))." nick names");
}

sub loaddb 
{ # load the players database
        backup();
        my ($l, $abc);
        %rps = ();
        if (!open(RPS,$opts{dbfile}) && -e $opts{dbfile}) {
                chatmsg("QUIT: loaddb() failed: $!", 1);
                close($sock);
        }
        while ($l=<RPS>) 
        {
                chomp($l);
                next if $l =~ /^#/; # skip comments
                my @i = split("\t",$l);
                if (scalar(@i) != DB_FIELDS) 
                {
                        chatmsg("QUIT: Anomaly in loaddb(); line $. of $opts{dbfile} has ".
                                "wrong fields (".scalar(@i).")");
                        debug("Anomaly in loaddb(); line $. of $opts{dbfile} has wrong ".
                                "fields (".scalar(@i).")",1);
                        print("Anomaly in loaddb(); line $. of $opts{dbfile} has wrong ".
                                "fields (".scalar(@i).")",1);
                        close($sock);
                        
                        print Dumper(@i)
                }
                
                my $u = lc($i[0]);
                ($rps{$u}{username},
                $rps{$u}{pass},
                $rps{$u}{isadmin},
                $rps{$u}{level},
                $rps{$u}{class},
                $rps{$u}{TTL},
                $rps{$u}{next},
                $rps{$u}{nick},
                $rps{$u}{loggedin},
                $abc,
                $rps{$u}{idled},
                $rps{$u}{x},
                $rps{$u}{y},
                $rps{$u}{created},
                $rps{$u}{lastseen},
                $rps{$u}{lastactivity},
                $rps{$u}{item}{amulet},
                $rps{$u}{item}{charm},
                $rps{$u}{item}{helm},
                $rps{$u}{item}{"pair of boots"},
                $rps{$u}{item}{"pair of gloves"},
                $rps{$u}{item}{ring},
                $rps{$u}{item}{"set of leggings"},
                $rps{$u}{item}{shield},
                $rps{$u}{item}{tunic},
                $rps{$u}{item}{weapon},
                $rps{$u}{alignment},
                $rps{$u}{gender},
                $rps{$u}{stat_combat},
                $rps{$u}{stat_strength},
                $rps{$u}{stat_endurance},
                $rps{$u}{stat_points},
                $rps{$u}{actionpoints},
                $rps{$u}{nextbonusactionpoint},
                ) = (@i[0..$#i]);
        }
        close(RPS);
        debug("loaddb(): loaded ".scalar(keys(%rps))." accounts");
}

sub moveplayers
{
        return unless $lasttime > 1;
        my $onlinecount = grep { isonline($_) } keys %rps;
        return unless $onlinecount;
        for (my $i=0;$i<$opts{self_clock};++$i) {
                # temporary hash to hold player positions, detect collisions
                my %positions = ();
                if ($quest{type} == 2 && @{$quest{questers}}) {
                        my $allgo = 1; # have all users reached <p1|p2>?
                        for (@{$quest{questers}}) {
                                if ($quest{stage}==1) {
                                        if ($rps{$_}{x} != $quest{p1}->[0] ||
                                                $rps{$_}{y} != $quest{p1}->[1]) {
                                                $allgo=0;
                                                last();
                                        }
                                }
                                else {
                                        if ($rps{$_}{x} != $quest{p2}->[0] ||
                                                $rps{$_}{y} != $quest{p2}->[1]) {
                                                $allgo=0;
                                                last();
                                        }
                                }
                        }
                        # all participants have reached point 1, now point 2
                        if ($quest{stage}==1 && $allgo) {
                                $quest{stage}=2;
                                $allgo=0; # have not all reached p2 yet
                        }
                        elsif ($quest{stage} == 2 && $allgo) {
                                chatmsg(clog("$rps{$quest{questers}->[0]}{username}, $rps{$quest{questers}->[1]}{username}, $rps{$quest{questers}->[2]}{username} and ".
                                                        "$rps{$quest{questers}->[3]}{username} have completed their ".
                                                         "journey! 25% of their burden is eliminated."));
                                for (@{$quest{questers}}) {
                                        $rps{$_}{next} = int($rps{$_}{next} * .75);
                                }
                                undef(@{$quest{questers}});
                                $quest{qtime} = time() + 21600; # next quest starts in 6 hours
                                $quest{type} = 1; # probably not needed
                                writequestfile();
                        }
                        else {
                                my(%temp,$player);
                                # load keys of %temp with online users
                                ++@temp{grep { isonline($_) } keys(%rps)};
                                # delete questers from list
                                delete(@temp{@{$quest{questers}}});
                                while ($player = each(%temp)) {
                                        $rps{$player}{x} += int(rand(3))-1;
                                        $rps{$player}{y} += int(rand(3))-1;
                                        # if player goes over edge, wrap them back around
                                        if ($rps{$player}{x} > $opts{mapx}) { $rps{$player}{x}=0; }
                                        if ($rps{$player}{y} > $opts{mapy}) { $rps{$player}{y}=0; }
                                        if ($rps{$player}{x} < 0) { $rps{$player}{x}=$opts{mapx}; }
                                        if ($rps{$player}{y} < 0) { $rps{$player}{y}=$opts{mapy}; }

                                        if (exists($positions{$rps{$player}{x}}{$rps{$player}{y}}) &&
                                                !$positions{$rps{$player}{x}}{$rps{$player}{y}}{battled}) {
                                                if ($rps{$positions{$rps{$player}{x}}{$rps{$player}{y}}{user}}{isadmin} &&
                                                        !$rps{$player}{isadmin} && rand(10) < 1) {
                                                        chatmsg("$rps{$player}{username} encounters ".
                                                           $rps{$positions{$rps{$player}{x}}{$rps{$player}{y}}{user}}{username}.
                                                                        " and bows humbly.");
                                                }
                                                if (rand($onlinecount) < 1) {
                                                        $positions{$rps{$player}{x}}{$rps{$player}{y}}{battled}=1;
                                                        collision_fight($player,
                                                                $positions{$rps{$player}{x}}{$rps{$player}{y}}{user});
                                                }
                                        }
                                        else {
                                                $positions{$rps{$player}{x}}{$rps{$player}{y}}{battled}=0;
                                                $positions{$rps{$player}{x}}{$rps{$player}{y}}{user}=$player;
                                        }
                                }
                                for (@{$quest{questers}}) {
                                        if ($quest{stage} == 1) {
                                                if (rand(100) < 1) {
                                                        if ($rps{$_}{x} != $quest{p1}->[0]) {
                                                                $rps{$_}{x} += ($rps{$_}{x} < $quest{p1}->[0] ?
                                                                                                1 : -1);
                                                        }
                                                        if ($rps{$_}{y} != $quest{p1}->[1]) {
                                                                $rps{$_}{y} += ($rps{$_}{y} < $quest{p1}->[1] ?
                                                                                                1 : -1);
                                                        }
                                                }
                                        }
                                        elsif ($quest{stage}==2) {
                                                if (rand(100) < 1) {
                                                        if ($rps{$_}{x} != $quest{p2}->[0]) {
                                                                $rps{$_}{x} += ($rps{$_}{x} < $quest{p2}->[0] ?
                                                                                                1 : -1);
                                                        }
                                                        if ($rps{$_}{y} != $quest{p2}->[1]) {
                                                                $rps{$_}{y} += ($rps{$_}{y} < $quest{p2}->[1] ?
                                                                                                1 : -1);
                                                        }
                                                }
                                        }
                                }
                        }
                }
                else {
                        for my $player (keys(%rps)) {
                                next unless isonline($player);
                                $rps{$player}{x} += int(rand(3))-1;
                                $rps{$player}{y} += int(rand(3))-1;
                                # if player goes over edge, wrap them back around
                                if ($rps{$player}{x} > $opts{mapx}) { $rps{$player}{x} = 0; }
                                if ($rps{$player}{y} > $opts{mapy}) { $rps{$player}{y} = 0; }
                                if ($rps{$player}{x} < 0) { $rps{$player}{x} = $opts{mapx}; }
                                if ($rps{$player}{y} < 0) { $rps{$player}{y} = $opts{mapy}; }
                                if (exists($positions{$rps{$player}{x}}{$rps{$player}{y}}) &&
                                        !$positions{$rps{$player}{x}}{$rps{$player}{y}}{battled}) {
                                        if ($rps{$positions{$rps{$player}{x}}{$rps{$player}{y}}{user}}{isadmin} &&
                                                !$rps{$player}{isadmin} && rand(10) < 1) {
                                                chatmsg("$rps{$player}{username} encounters ".
                                                   $positions{$rps{$player}{x}}{$rps{$player}{y}}{user}.
                                                                " and bows humbly.");
                                        }
                                        if (rand($onlinecount) < 1) {
                                                $positions{$rps{$player}{x}}{$rps{$player}{y}}{battled}=1;
                                                collision_fight($player,
                                                        $positions{$rps{$player}{x}}{$rps{$player}{y}}{user});
                                        }
                                }
                                else {
                                        $positions{$rps{$player}{x}}{$rps{$player}{y}}{battled}=0;
                                        $positions{$rps{$player}{x}}{$rps{$player}{y}}{user}=$player;
                                }
                        }
                }
        }
}

sub mksalt { # generate a random salt for passwds
        join '',('a'..'z','A'..'Z','0'..'9','/','.')[rand(64), rand(64)];
}

sub chatmsg 
{ # send a message to the chat
        my $msg = shift or return undef;
        my $skipq = shift;
        if ($silentmode > 0) { return undef; }
        #sts("SEND:CHAT:1;$msg");
        
        while (length($msg))
        {
                my $msgpart = substr($msg,0, MAXMESSAGELENGTH);
                chatlog(gamelog("C 1:".prependSpaces($primnick, LOGNICKLENGTH)."> $msgpart"));
                sts("SEND:CHAT:1;$msgpart", $skipq);
                #sts("SEND:CHAT:2;$msgpart", 0) if (exists($opts{mirrorchat}) && length($opts{mirrorchat}) > 0);
                substr($msg,0, MAXMESSAGELENGTH)="";
        }
}

sub privmsg
{ # send a message to an arbitrary entity
# SEND:PRIV:name:msg
        my $msg = shift or return undef;
        my $target = shift or return undef;
        my $skipq = shift;
        
        if ($silentmode >= 2)
        {
                return undef;
        }
        
        while (length($msg)) 
        {
                my $msgpart = substr($msg,0, MAXMESSAGELENGTH);
                gamelog("  :$target: $msgpart");
                sts("SEND:PRIV:$target:$msgpart", $skipq);
                substr($msg,0, MAXMESSAGELENGTH)="";
        }
}

sub cmdresponse
{ # send a message to an arbitrary entity, if the response is from a cmd the user used
# SEND:PRIV:name:msg
        my $msg = shift or return undef;
        my $target = shift or return undef;
        my $skipq = shift;
        
        if ($silentmode >= 3)
        {
                #THIS SHOULD NEVER HAPPEN, cmdresponse() should never be called if the silent mode is 2 or 3
                debug("Error! Look in sub cmdresponse");
                return undef;
        }
        
        while (length($msg)) 
        {
                my $msgpart = substr($msg,0, MAXMESSAGELENGTH);
                gamelog("  :$target:$msgpart");
                sts("SEND:PRIV:$target:$msgpart", $skipq);
                substr($msg,0, MAXMESSAGELENGTH)="";
        }
}

sub help 
{ # print help message
        (my $prog = $0) =~ s/^.*\///;

        print "
usage: $prog [OPTIONS]
  --help, -h                   Print this message

";
}

sub itemsum 
{
        my $user = shift;
        # is this for a battle? if so, good users get a 10% boost and evil users get
        # a 10% detriment
        my $battle = shift;
        return -1 unless defined $user;
        
        if (lc($user) eq lc($primnick))
        {
                my $sum = 0;
                my $newsum = 0;
                for my $u (keys(%rps))
                {
                        if (lc($u) ne lc($primnick))
                        {
                                $newsum = itemsum($u);
                                $sum = $newsum if $sum < $newsum;
                        }
                }
                return $sum + 1;
        }

        if (!exists($rps{$user})) { return -1; }
        
        my $sum = 0;
        my $tmp = "";
        
        foreach (keys(%{$rps{$user}{item}}))
        {
                $tmp = $rps{$user}{item}{$_};
                $tmp =~ s/\D//g;
                $sum += lvl($tmp);
        }
        
        if ($battle) 
        {
                return $rps{$user}{alignment} eq 'e' ? int($sum*.9) :
                           $rps{$user}{alignment} eq 'g' ? int($sum*1.1) :
                           $sum;
        }
        return $sum;
}

sub daemonize() 
{
        # win32 doesn't daemonize (this way?)
        if ($^O eq "MSWin32")
        {
                print debug("Nevermind, this is Win32")."\n";
                return;
        }
        use POSIX 'setsid';
        $SIG{CHLD} = sub { };
        fork() && exit(0); # kill parent
        POSIX::setsid() || debug("POSIX::setsid() failed: $!",1);
        $SIG{CHLD} = sub { };
        fork() && exit(0); # kill the parent as the process group leader
        $SIG{CHLD} = sub { };
        open(STDIN,'/dev/null') || debug("Cannot read /dev/null: $!",1);
        open(STDOUT,'>/dev/null') || debug("Cannot write to /dev/null: $!",1);
        open(STDERR,'>/dev/null') || debug("Cannot write to /dev/null: $!",1);
        # write our PID to $opts{pidfile}, or return semi-silently on failure
        open(PIDFILE,">$opts{pidfile}") || do {
                debug("Error: failed opening pid file: $!");
                return;
        };
        print PIDFILE $$;
        close(PIDFILE);
}

sub calamity { # suffer a little one
        my @players = grep { isonline($_) } keys(%rps);
        return unless @players;
        my $player = $players[rand(@players)];
        if (rand(10) < 1) {
                my @items = ("amulet","charm","weapon","tunic","set of leggings",
                                         "shield");
                my $type = $items[rand(@items)];
                if ($type eq "amulet") {
                        chatmsg(clog("$rps{$player}{username} fell, chipping the stone in ".g($player, "his", "her")." amulet! ".
                                                 "$rps{$player}{username}\'s $type loses 10% of its effectiveness."));
                }
                elsif ($type eq "charm") {
                        chatmsg(clog("$rps{$player}{username} slipped and dropped ".g($player, "his", "her")." charm in a dirty ".
                                                 "bog! $rps{$player}{username}\'s $type loses 10% of its ".
                                                 "effectiveness."));
                }
                elsif ($type eq "weapon") {
                        chatmsg(clog("$rps{$player}{username} left ".g($player, "his", "her")." weapon out in the rain to rust! ".
                                                 "$rps{$player}{username}\'s $type loses 10% of its effectiveness."));
                }
                elsif ($type eq "tunic") {
                        chatmsg(clog("$rps{$player}{username} spilled a level 7 shrinking potion on ".g($player, "his", "her")." ".
                                                 "tunic! $rps{$player}{username}\'s $type loses 10% of its ".
                                                 "effectiveness."));
                }
                elsif ($type eq "shield") {
                        chatmsg(clog("$rps{$player}{username}\'s shield was damaged by a dragon's fiery ".
                                                 "breath! $rps{$player}{username}\'s $type loses 10% of its ".
                                                 "effectiveness."));
                }
                elsif ($type eq "set of leggings") {
                        chatmsg(clog("$rps{$player}{username} burned a hole through ".g($player, "his", "her")." leggings while ".
                                                 "ironing them! $rps{$player}{username}\'s $type loses 10% of its ".
                                                 "effectiveness."));
                }
                else
                {
                        debug("Unknown item in calamity()");
                        return;
                }
                my $suffix="";
                if ($rps{$player}{item}{$type} =~ /^\s*\d+(.*)$/) { $suffix=$1; }
                $rps{$player}{item}{$type} = int(lvl($rps{$player}{item}{$type}) * .9);
                $rps{$player}{item}{$type}.=$suffix;
        }
        else {
                my $time = int(int(5 + rand(8)) / 100 * $rps{$player}{TTL} * 0.2);
                if (!open(Q,$opts{eventsfile})) {
                        return chatmsg("ERROR: Failed to open $opts{eventsfile}: $!");
                }
                my($i,$actioned);
                while (my $line = <Q>) {
                        chomp($line);
                        if ($line =~ /^C (.*)/ && rand(++$i) < 1) { $actioned = $1; }
                }
                $time = updateTTL($player, $time);
                chatmsg(clog("$rps{$player}{username} $actioned. This terrible calamity has slowed ".
                                         "them ".duration($time)." from level ".
                                         ($rps{$player}{level}+1)."."));
                chatmsg("$rps{$player}{username} reaches next level in ".duration($rps{$player}{next}).".");
        }
}

sub godsend { # bless the unworthy
        my @players = grep { isonline($_) } keys(%rps);
        return unless @players;
        my $player = $players[rand(@players)];
        if (rand(10) < 1) {
                my @items = ("amulet","charm","weapon","tunic","set of leggings",
                                         "shield");
                my $type = $items[rand(@items)];
                if ($type eq "amulet") {
                        chatmsg(clog("$rps{$player}{username}\'s amulet was blessed by a passing cleric! ".
                                                 "$rps{$player}{username}\'s $type gains 10% effectiveness."));
                }
                elsif ($type eq "charm") {
                        chatmsg(clog("$rps{$player}{username}\'s charm ate a bolt of lightning! ".
                                                 "$rps{$player}{username}\'s $type gains 10% effectiveness."));
                }
                elsif ($type eq "weapon") {
                        chatmsg(clog("$rps{$player}{username} sharpened the edge of ".g($player, "his", "her")." weapon! ".
                                                 "$rps{$player}{username}\'s $type gains 10% effectiveness."));
                }
                elsif ($type eq "tunic") {
                        chatmsg(clog("A magician cast a spell of Rigidity on $rps{$player}{username}\'s ".
                                                 "tunic! $rps{$player}{username}\'s $type gains 10% effectiveness."));
                }
                elsif ($type eq "shield") {
                        chatmsg(clog("$rps{$player}{username} reinforced ".g($player, "his", "her")." shield with a dragon's ".
                                                 "scales! $rps{$player}{username}\'s $type gains 10% effectiveness."));
                }
                else {
                        chatmsg(clog("The local wizard imbued $rps{$player}{username}\'s pants with a ".
                                                 "Spirit of Fortitude! $rps{$player}{username}\'s $type gains 10% ".
                                                 "effectiveness."));
                }
                my $suffix="";
                if ($rps{$player}{item}{$type} =~ /(\D)$/) { $suffix=$1; }
                $rps{$player}{item}{$type} = int(lvl($rps{$player}{item}{$type}) * 1.1);
                $rps{$player}{item}{$type}.= $suffix;
        }
        else {
                my $time = int(int(5 + rand(8)) / 100 * $rps{$player}{TTL} * 0.2);
                my $actioned;
                if (!open(Q,$opts{eventsfile})) {
                        return chatmsg("ERROR: Failed to open $opts{eventsfile}: $!");
                }
                my $i;
                while (my $line = <Q>) {
                        chomp($line);
                        if ($line =~ /^G (.*)/ && rand(++$i) < 1) {
                                $actioned = $1;
                        }
                }
                $time = updateTTL($player, -$time);
                chatmsg(clog("$rps{$player}{username} $actioned! This wondrous godsend has ".
                                         "accelerated them ".duration($time)." towards level ".
                                         ($rps{$player}{level}+1)."."));
                chatmsg("$rps{$player}{username} reaches next level in ".duration($rps{$player}{next}).".");
        }
}

sub quest 
{
        my ($minonlinetime) = @_;
        $minonlinetime = 3600 unless (defined($minonlinetime));
        @{$quest{questers}} = grep { isonline($_) && $rps{$_}{level} > 39 && $rps{$_}{joinedarena} && 
                                                                 time() - $rps{$_}{joinedarena} > $minonlinetime } keys(%rps);
        if (@{$quest{questers}} < 4) { return undef(@{$quest{questers}}); }
        while (@{$quest{questers}} > 4) {
                splice(@{$quest{questers}},int(rand(@{$quest{questers}})),1);
        }
        if (!open(Q,$opts{eventsfile})) {
                return chatmsg("ERROR: Failed to open $opts{eventsfile}: $!");
        }
        my $i;
        while (my $line = <Q>) {
                chomp($line);
                if ($line =~ /^Q/ && rand(++$i) < 1) {
                        if ($line =~ /^Q1 (.*)/) {
                                $quest{text} = $1;
                                $quest{type} = 1;
                                $quest{qtime} = time() + 7200 + int(rand(7200)); # 2-4 hours
                        }
                        elsif ($line =~ /^Q2 (\d+) (\d+) (\d+) (\d+) (.*)/) {
                                $quest{p1} = [$1,$2];
                                $quest{p2} = [$3,$4];
                                $quest{text} = $5;
                                $quest{type} = 2;
                                $quest{stage} = 1;
                        }
                }
        }
        close(Q);
        if ($quest{type} == 1) {
                chatmsg("$rps{$quest{questers}->[1]}{username}, $rps{$quest{questers}->[2]}{username} and ".
                                "$rps{$quest{questers}->[3]}{username} have been chosen by the gods to ".
                                "$quest{text}. Quest to end in ".duration($quest{qtime}-time()).
                                ".");
        }
        elsif ($quest{type} == 2) {
                chatmsg("$rps{$quest{questers}->[1]}{username}, $rps{$quest{questers}->[2]}{username} and ".
                                "$rps{$quest{questers}->[3]}{username} have been chosen by the gods to ".
                                "$quest{text}. Participants must first reach [$quest{p1}->[0],".
                                "$quest{p1}->[1]], then [$quest{p2}->[0],$quest{p2}->[1]].".
                                ($opts{mapurl}?" See $opts{mapurl} to monitor their journey's ".
                                "progress.":""));
        }
        writequestfile();
}

sub questpencheck {
        my $k = shift;
        my ($quester,$player);
        for $quester (@{$quest{questers}}) 
        {
                if ($quester eq $k) 
                {
                        chatmsg(clog("$rps{$k}{username}\'s prudence and self-regard has brought the ".
                                                 "wrath of the gods upon the realm. All your great ".
                                                 "wickedness makes you as it were heavy with lead, ".
                                                 "and to tend downwards with great weight and ".
                                                 "pressure towards hell. Therefore have you drawn ".
                                                 "yourselves 15 steps closer to that gaping maw."));
                        for $player (grep { isonline($_) } keys %rps) 
                        {
                                my $gain = getPen($rps{$player}{level}, 15);
                        }
                        undef(@{$quest{questers}});
                        $quest{qtime} = time() + 43200; # 12 hours
                }
        }
}

sub clog {
        my $mesg = shift;
        open(B,">>$opts{modsfile}") or do {
                debug("Error: Cannot open $opts{modsfile}: $!");
                chatmsg("Error: Cannot open $opts{modsfile}: $!");
                return $mesg;
        };
        print B ts()."$mesg\n";
        close(B);
        return $mesg;
}

sub backup() 
{
        if (! -d ".dbbackup/") { mkdir(".dbbackup",0700); }
        if ($^O ne "MSWin32") 
        {
                system("cp $opts{dbfile} .dbbackup/$opts{dbfile}".time());
        }
        else 
        {
                system("copy $opts{dbfile} .dbbackup\\$opts{dbfile}".time());
        }
        debug(gamelog("*** BACKUP: $opts{dbfile} copied to dbbackup/$opts{dbfile}".time()));
}

sub penalize {
        my $username = shift;
        return 0 if !defined($username);
        return 0 if !exists($rps{$username});
        my $type = shift;
        my $pen = 0;
        
        
        questpencheck($username) if ($type eq "quit" || $type eq "logout");
        
        if ($type eq "resetstats")
        {
                $pen = getPen(($rps{$username}{level} < 50 ? $rps{$username}{level} : 50), 10);
                if ($opts{limitpen} && $pen > $opts{limitpen}) 
                {
                        $pen = $opts{limitpen};
                }
                $pen = updateTTL($username, $pen);

                privmsg("Penalty of ".duration($pen)." added to your timer for resetting your stats.", $rps{$username}{nick});
        }
        elsif ($type eq "classchange")
        {
                $pen = getPen($rps{$username}{level}, 50);
                if ($opts{limitpen} && $pen > $opts{limitpen}) 
                {
                        $pen = $opts{limitpen};
                }
                $pen = updateTTL($username, $pen);
                
                privmsg("Penalty of ".duration($pen)." added to your timer for changing your class.", $rps{$username}{nick});
        }   
#        elsif ($type eq "quit") 
#        {
#                 $pen = getPen($rps{$username}{level}, 20);
#                 if ($opts{limitpen} && $pen > $opts{limitpen}) {
#                         $pen = $opts{limitpen};
#                 }
#                 $pen = updateTTL($username, $pen);

#                 privmsg("Penalty of ".duration($pen)." added to your timer for ".
#                            "leaving the arena.",$rps{$username}{nick});
#                 chatmsg("$rps{$username}{username} has logged out");
#        }
#         elsif ($type eq "privmsg") 
#         {
#                 #$pen = int(shift(@_) * ($opts{rppenstep}**$rps{$username}{level}));
#                 $pen = getPen($rps{$username}{level}, shift(@_));
#                 if ($opts{limitpen} && $pen > $opts{limitpen}) {
#                         $pen = $opts{limitpen};
#                 }
#                 $pen = updateTTL($username, $pen);
#                 privmsg("Penalty of ".duration($pen)." added to your timer for talking in the chat",$rps{$username}{nick});
#         }
        elsif ($type eq "logout")
        {
                #$pen = int(20 * ($opts{rppenstep}**$rps{$username}{level}));
                $pen = getPen($rps{$username}{level}, 20);
                if ($opts{limitpen} && $pen > $opts{limitpen}) {
                        $pen = $opts{limitpen};
                }
                $pen = updateTTL($username, $pen);
                privmsg("Penalty of ".duration($pen)." added to your timer for ".
                    "LOGOUT command.",$rps{$username}{nick});
                chatmsg("$rps{$username}{username} has logged out");
        }
        return 1; # successfully penalized a user! woohoo!
}

sub logToFile
{
        my $file = shift;
        (my $text = shift) =~ s/[\r\n]//g;
        return $text unless ($file && $text);
        
        my @ts = localtime(time());
        my @gm = gmtime(time());
        
        my $fileExists = -e $file;
        open(LOG,">>$file") or do
        {
                return $text;
        };
        
        if (!$fileExists)
        {
                my @gm = gmtime(time());
                print LOG sprintf("GMT  : %02d/%02d/%02d %02d:%02d:%02d\n", $gm[3],$gm[4]+1,$gm[5]%100,$gm[2],$gm[1],$gm[0]);
                print LOG sprintf("Local: %02d/%02d/%02d %02d:%02d:%02d (DST: %1d)\n", $ts[3],$ts[4]+1,$ts[5]%100,$ts[2],$ts[1],$ts[0],$ts[8]);
        }
        
        print LOG ts()."$text\n";
        close(LOG);
        
        return $text;
}

sub alllog
{
        my $text = shift;
        gamelog($text);
        chatlog($text);
        arenalog($text);
        debug($text);
        return $text;
}

# Private Messages + messages from chat
sub gamelog
{
        my $text = shift;
        my @ts = localtime(time());
        
        if ($opts{gamelogfolder})
        {
                logToFile($opts{gamelogfolder} . "/" . sprintf("%02d-%02d-%02d-game.txt", $ts[3], $ts[4]+1, $ts[5]+1900), $text);
        }
        
        return $text;
}

# Messages from chat
sub chatlog
{
        my $text = shift;
        my @ts = localtime(time());
        
        if ($opts{chatlogfolder})
        {
                logToFile($opts{chatlogfolder} . "/" . sprintf("%02d-%02d-%02d-chat.txt", $ts[3], $ts[4]+1, $ts[5]+1900), $text);
        }
        
        return $text;
}

# Public chat + freq chat, arena messages
sub arenalog
{
        my $text = shift;
        my @ts = localtime(time());
        
        if ($opts{arenalogfolder})
        {
                logToFile($opts{arenalogfolder} . "/" . sprintf("%02d-%02d-%02d-arena.txt", $ts[3], $ts[4]+1, $ts[5]+1900), $text);
        }
        
        return $text;
}

sub debug 
{
        my $text = shift;
        my $die = shift;
        my @ts = localtime(time());
        
        if ($opts{debugfolder} && $opts{debug})
        {
                logToFile($opts{debugfolder} . "/" . sprintf("%02d-%02d-%02d-debug.txt", $ts[3], $ts[4]+1, $ts[5]+1900), $text);
        }
        
        die("$text\n") if ($die);
        
        return $text;
}

sub finduser 
{
        my $nick = shift;
        return undef if !defined($nick);
        for my $user (keys(%rps)) {
                next unless isonline($user);
                if ($rps{$user}{nick} eq $nick) { return $user; }
        }
        return undef;
}

sub isonline 
{ # return 0/1 if username is online
        my $user = shift;
        
        if (!defined($user))
        {
                return 0;
        }
        
        if (!exists($rps{$user})) 
        {
                return 0;
        }
        
        if ($rps{$user}{loggedin})
        {
                if ($opts{owneralwaysonline} && $opts{owner} eq $user)
                {
                        $rps{$user}{lastseen} = time();
                        return 1;
                }
                
                if ($onarena{$rps{$user}{nick}})
                {
                        $rps{$user}{lastseen} = time();
                        return 1;
                }
                
                
                if ($onchat{$rps{$user}{nick}})
                {
                        $rps{$user}{lastseen} = time();
                        return 1;
                }
        }
}

sub ha 
{ # return 0/1 if username has access
        my $user = shift;
        if (!defined($user)) {
                debug("Error: Attempted ha(undefined) for invalid username");
                return 0;
        }
        elsif (!exists($rps{$user})) {
                debug("Error: Attempted ha(\"$user\") for invalid username");
                return 0;
        }
        
        #make sure the admin is also in this arena to prevent malicious stuff
        #if ($rps{$user}{isadmin} && $onarena{$rps{$user}{nick}})
        if ($rps{$user}{isadmin})
        {
                return 1;
        }
        
        return 0;
}

sub writequestfile 
{
        return unless $opts{writequestfile};
        open(QF,">$opts{questfilename}") or do {
                chatmsg("Error: Cannot open $opts{questfilename}: $!");
                return;
        };
        # if no active quest, just empty questfile. otherwise, write it
        if (@{$quest{questers}}) {
                if ($quest{type}==1) {
                        print QF "T $quest{text}\n".
                                         "Y 1\n".
                                         "S $quest{qtime}\n".
                                         "P1 $quest{questers}->[0]\n".
                                         "P2 $quest{questers}->[1]\n".
                                         "P3 $quest{questers}->[2]\n".
                                         "P4 $quest{questers}->[3]\n";
                }
                elsif ($quest{type}==2) {
                        print QF "T $quest{text}\n".
                                         "Y 2\n".
                                         "S $quest{stage}\n".
                                         "P $quest{p1}->[0] $quest{p1}->[1] $quest{p2}->[0] ".
                                                "$quest{p2}->[1]\n".
                                         "P1 $quest{questers}->[0] $rps{$quest{questers}->[0]}{x} ".
                                                 "$rps{$quest{questers}->[0]}{y}\n".
                                         "P2 $quest{questers}->[1] $rps{$quest{questers}->[1]}{x} ".
                                                 "$rps{$quest{questers}->[1]}{y}\n".
                                         "P3 $quest{questers}->[2] $rps{$quest{questers}->[2]}{x} ".
                                                 "$rps{$quest{questers}->[2]}{y}\n".
                                         "P4 $quest{questers}->[3] $rps{$quest{questers}->[3]}{x} ".
                                                 "$rps{$quest{questers}->[3]}{y}\n";
                }
        }
        close(QF);
}

sub g
{
        my ($u, $male, $female) = @_;
        return $rps{$u}{gender} eq "f" ? $female : $male;
}

sub goodness {
        my @players = grep { $rps{$_}{alignment} eq "g" &&
                                                 isonline($_) } keys(%rps);
        return unless @players > 1;
        splice(@players,int(rand(@players)),1) while @players > 2;
        
        if (int(rand(2)) < 1)
        {
                my $gain = 15 + int(rand(8));
                chatmsg(clog("$rps{$players[0]}{username} and $rps{$players[1]}{username} have not let the iniquities of ".
                                         "evil men poison them. Together have they prayed to their ".
                                         "god, and it is her light that now shines upon them. $gain\% ".
                                         "of their time is removed from their clocks."));
                                         
                my ($next1, $next2);
                
                if ($rps{$players[0]}{next} > 0)
                {
                        $next1 = int($rps{$players[0]}{next}*(1 - ($gain/100)));
                        $next2 = int($rps{$players[0]}{TTL}*(1 - ($gain/100)));
                        $rps{$players[0]}{next} = $next1 > $next2 ? $next2 : $next1;
                }
                
                if ($rps{$players[1]}{next} > 0)
                {
                        $next1 = int($rps{$players[1]}{next}*(1 - ($gain/100)));
                        $next2 = int($rps{$players[1]}{TTL}*(1 - ($gain/100)));
                        $rps{$players[1]}{next} = $next1 > $next2 ? $next2 : $next1;
                }
                chatmsg("$rps{$players[0]}{username} reaches next level in ".
                                duration($rps{$players[0]}{next}).".");
                chatmsg("$rps{$players[1]}{username} reaches next level in ".
                                duration($rps{$players[1]}{next}).".");
        }
        else
        {
                my $type = $possible_items[rand(@possible_items)];
                my $gain = 3 + int(rand(7));
                chatmsg(clog("$rps{$players[0]}{username} and $rps{$players[1]}{username} have not let the iniquities of ".
                             "evil men poison them. Together have they prayed to their ".
                             "god, and it is her light that empowers their $type to increase by $gain\% in strength."
                       ));
                my $suffix="";
                
                if ($rps{$players[0]}{item}{$type} =~ /^\s*\d+(.*)$/) { $suffix=$1; }
                $rps{$players[0]}{item}{$type}  = round(lvl($rps{$players[0]}{item}{$type}) + (lvl($rps{$players[0]}{item}{$type}) * ($gain/100)));
                $rps{$players[0]}{item}{$type} .= $suffix;
                
                $suffix="";
                if ($rps{$players[1]}{item}{$type} =~ /(\D)$/) { $suffix=$1; }
                $rps{$players[1]}{item}{$type} = round(lvl($rps{$players[1]}{item}{$type}) + (lvl($rps{$players[1]}{item}{$type}) * ($gain/100)));
                $rps{$players[1]}{item}{$type} .= $suffix;
        }
}

sub evilness {
        my @evil = grep { $rps{$_}{alignment} eq "e" &&
                                          isonline($_) } keys(%rps);
        return unless @evil;
        my $me = $evil[rand(@evil)];
        
        # evil only steals from good and evil :^(
        #my @good = grep { $rps{$_}{alignment} ne "n" && isonline($_) && $_ ne $opts{owner}} keys(%rps);
        my @good = grep { $rps{$_}{alignment} ne "n" && isonline($_)} keys(%rps);
        
        return unless @good; #dont do any evil actions if there are no good players
        
        if (int(rand(2)) < 1)
        {                        
                my $target = $good[rand(@good)];
                #my @items = ("ring","amulet","charm","weapon","helm","tunic",
                #                         "pair of gloves","set of leggings","shield",
                #                         "pair of boots");
                my $type = $possible_items[rand(@possible_items)];
                if (lvl($rps{$target}{item}{$type}) > lvl($rps{$me}{item}{$type})) {
                        my $tempitem = $rps{$me}{item}{$type};
                        $rps{$me}{item}{$type} = $rps{$target}{item}{$type};
                        $rps{$target}{item}{$type} = $tempitem;
                        chatmsg(clog("$rps{$me}{username} stole $rps{$target}{username}\'s level ".
                                                 lvl($rps{$me}{item}{$type})." $type while they were ".
                                                 "sleeping! $rps{$me}{username} leaves ".g($me, "his", "her")." old level ".
                                                 lvl($rps{$target}{item}{$type})." $type behind, ".
                                                 "which $rps{$target}{username} then takes."));
                }
                else {
                        privmsg("You made to steal $rps{$target}{username}\'s $type, but realized it was ".
                                   "lower level than your own. You creep back into the ".
                                   "shadows.",$rps{$me}{nick});
                }
        }
        else 
        { # being evil only pays about half of the time...
                my $gain = 1 + int(rand(5));
                my $time = int($rps{$me}{TTL} * ($gain/100));
                
                chatmsg(clog("$rps{$me}{username} is forsaken by ".g($me, "his", "her")." evil god. ".
                                         duration($time)." is added ".
                                         "to ".g($me, "his", "her")." clock."));
                
                updateTTL($me, $time);
                chatmsg("$rps{$me}{username} reaches next level in ".duration($rps{$me}{next}).".");
        }
}

sub fisher_yates_shuffle {
        my $array = shift;
        my $i;
        for ($i = @$array; --$i; ) {
                my $j = int rand ($i+1);
                next if $i == $j;
                @$array[$i,$j] = @$array[$j,$i];
        }
}

sub writedb 
{
        open(RPS,">$opts{dbfile}") or do 
        {
                chatmsg("ERROR: Cannot write $opts{dbfile}: $!");
                return 0;
        };
        print RPS join("\t","# username",
                        "pass",
                        "is admin",
                        "level",
                        "class",
                        "ttl",
                        "next ttl",
                        "nick",
                        "logged in",
                        "online",
                        "idled",
                        "x pos",
                        "y pos",
                        "created",
                        "last seen",
                        "last activity",
                        "amulet",
                        "charm",
                        "helm",
                        "boots",
                        "gloves",
                        "ring",
                        "leggings",
                        "shield",
                        "tunic",
                        "weapon",
                        "alignment",
                        "gender",
                        "stat_combat",
                        "stat_strength",
                        "stat_endurance",
                        "stat_points",
                        "actionpoints",
                        "nextbonusactionpoint",
                        )."\n";
        my $k;
        keys(%rps); # reset internal pointer
        while ($k = each(%rps)) 
        {
                if (exists($rps{$k}{next}) && defined($rps{$k}{next}) && lc($k) ne lc($primnick))
                {
                        print RPS join("\t", 
                                        $rps{$k}{username},
                                        $rps{$k}{pass},
                                        $rps{$k}{isadmin},
                                        $rps{$k}{level},
                                        $rps{$k}{class},
                                        $rps{$k}{TTL},
                                        $rps{$k}{next},
                                        $rps{$k}{nick},
                                        $rps{$k}{loggedin},
                                        (isonline($k) ? 1 : 0),
                                        $rps{$k}{idled},
                                        $rps{$k}{x},
                                        $rps{$k}{y},
                                        $rps{$k}{created},
                                        $rps{$k}{lastseen},
                                        $rps{$k}{lastactivity},
                                        $rps{$k}{item}{amulet},
                                        $rps{$k}{item}{charm},
                                        $rps{$k}{item}{helm},
                                        $rps{$k}{item}{"pair of boots"},
                                        $rps{$k}{item}{"pair of gloves"},
                                        $rps{$k}{item}{ring},
                                        $rps{$k}{item}{"set of leggings"},
                                        $rps{$k}{item}{shield},
                                        $rps{$k}{item}{tunic},
                                        $rps{$k}{item}{weapon},
                                        $rps{$k}{alignment},
                                        $rps{$k}{gender},
                                        $rps{$k}{stat_combat},
                                        $rps{$k}{stat_strength},
                                        $rps{$k}{stat_endurance},
                                        $rps{$k}{stat_points},
                                        $rps{$k}{actionpoints},
                                        $rps{$k}{nextbonusactionpoint}
                                        )
                                        ."\n";
                }
        }
        close(RPS);
}

sub readconfig 
{
        if (! -e ".irpg.conf") {
                debug("Error: Cannot find .irpg.conf. Copy it to this directory, ".
                          "please.",1);
        }
        else {
                open(CONF,"<.irpg.conf") or do {
                        debug("Failed to open config file .irpg.conf: $!",1);
                };
                my($line,$key,$val);
                while ($line=<CONF>) 
                {
                        next() if $line =~ /^#/; # skip comments
                        $line =~ s/[\r\n]//g;
                        $line =~ s/^\s+//g;
                        next() if !length($line); # skip blank lines
                        ($key,$val) = split(/\s+/,$line,2);
                        $key = lc($key);
                        if (lc($val) eq "on" || lc($val) eq "yes") { $val = 1; }
                        elsif (lc($val) eq "off" || lc($val) eq "no") { $val = 0; }
                        if ($key eq "die") {
                                die("Please edit the file .irpg.conf to setup your bot's ".
                                        "options. Also, read the README file if you haven't ".
                                        "yet.\n");
                        }
                        elsif ($key eq "server") { push(@{$opts{servers}},$val); }
                        elsif ($key eq "okurl") { push(@{$opts{okurl}},$val); }
                        else { $opts{$key} = $val; }
                }
                close(CONF);
                $opts{owner} = lc($opts{owner}) if ($opts{owner});
        }
}

sub trim
{
        my $string = shift;
        $string =~ s/^\s+//;
        $string =~ s/\s+$//;
        return $string;
}

sub round
{
        return int($_[0] + .5 * ($_[0] <=> 0));
}

sub lvl
{
        no warnings;
        return int($_[0]); # 50d -> 50
}

sub cleanup
{
        writedb(); # most important, comes first
        $opts{reconnect} = 0;
        chatmsg("DIE: Received SIGNAL",1);
        close($sock);
        alllog("*** DIE: Received SIGNAL");
        die("Received Signal");
}