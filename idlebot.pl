#!/usr/local/bin/perl -w

use strict;
use warnings;
use IO::Socket;
use IO::Select;
use Time::HiRes qw ( time alarm sleep );   # Need floating points for time() and sleep()
use constant ASSS_MAXMESSAGELENGTH => 249; # chat.c:153 ASSS 1.4.4

my %config = 
(
        server_ip => '67.19.122.82',    # IP/Hostname of the server
        server_port => '5000',          # Port of the server
        server_arena => '0',            # Arena to join
        server_reconnectdelay => 20,    # How many seconds to wait between reconnects when disconnected, 0 to disabled reconnect. (float)
        
        bot_nick => 'Partyraiser',      # Subspace nickname of the bot
        bot_password => '[REMOVED]',    # Password of the bot
        bot_chat => 'irpg',             # ?chat= to send upon join
        
        admin_nick => 'JoWie',          # The subspace nick of the administrator that may execute certain commands
        
        queue_maxqueue => 25,           # Maximum items in queue before auto clear (last resort), 0 to disable.
        queue_delay => 0.5,             # How many seconds to wait between lines to send from queue (float)
        queue_lines => 2,               # How many lines to send every queue_delay
        
        log_raw => 1,                   # Log incomming and outgoing raw chatnet messages
        log_file => 'log.txt'           # File to log to
);

###############################################################

my $sock = 0;   # our socket
my $sel = 0;    # our select
my $buffer;     # buffer for socket stuff

my $bot_nick;   # Current bot_nick

# Detect lag outs, etc
my $lastmessage_received = 0;
my $lastmessage_sent = 0;
my $pingsent = 0;               # Has a ping been sent? (private message to the bot itself)

my @queue; # outgoing message queue
my $queue_lasttime = 0; # last time fq() was called

###############################################################



$SIG{INT} = 'sigint'; # Pressing CTRL-C in the console, closing console, etc
$SIG{TERM} = 'sigint';

print("\n");
logm("IdleBot for Chatnet starting...");

CONNECT:
while (!$sock) # keep trieng to connect
{
        logm("Connecting to $config{server_ip}:$config{server_port}.");
        my %sockinfo = (PeerAddr => $config{server_ip}, PeerPort => $config{server_port});

        $sock = IO::Socket::INET->new(%sockinfo) or logm("Error: failed to connect to: $!.\n");
        
        if ($sock)
        {
                logm("Connected.");
        }
        else
        {
                sleep($config{server_reconnectdelay});       # Wait 10 seconds before next connection attempt
        }
}

# Connected after this point

$sel = IO::Select->new($sock);

sts("LOGIN:1;IdleBot:$config{bot_nick}:$config{bot_password}", 1);

while (1)
{
        my($readable) = IO::Select->select($sel, undef, undef,0.5);
        
        if (defined($readable)) 
        {
                my $fh = $readable->[0];
                my $buffer2;
                $fh->recv($buffer2, 512, 0);
                
                if (length($buffer2)) 
                {        # received something
                
                        
                        $lastmessage_received = time();
                        $pingsent = 0;
                    
                        $buffer .= $buffer2;
                        
                        while (index($buffer, "\n") != -1) 
                        {
                                my $line = substr($buffer,0,index($buffer,"\n")+1);
                                $buffer = substr($buffer,length($line));
                                parse(trim($line));
                        }
                }
                else
                {
                        # Disconnected from server
                        
                        close($fh);
                        $sel->remove($fh);

                        if ($config{server_reconnectdelay})
                        {
                                undef(@queue);
                                undef($sock);
                                logm("Socket closed; disconnected. Cleared outgoing message ".
                                          "queue. Waiting $config{server_reconnectdelay}s before next ".
                                          "connection attempt...");
                                sleep($config{server_reconnectdelay});
                                goto CONNECT;
                        }
                        else { logm("Socket closed; disconnected."); }
                }
        }
        else 
        { 
                select(undef, undef, undef,1); 
        }
        
        if ((time() - $queue_lasttime) >= $config{queue_delay}) 
        {
                fq();
        }
        
        lagoutcheck();
}

sub lagoutcheck
{
        if (((time() - $lastmessage_sent) > 20) && ($lastmessage_sent > 0)) 
        {
                $lastmessage_sent = time(); #make sure no flood is caused if something goes wrong in sts()
                sts("NOOP", 1);
        }
        
        if (((time() - $lastmessage_received) > 15) && ($pingsent == 0) && ($lastmessage_received > 0)) 
        {
                $pingsent = 1;
                sts("SEND:PRIV:$bot_nick:PING");
        }
        
        if (((time() - $lastmessage_received) > 30) && ($lastmessage_received > 0)) 
        {
                # connection must have died...
                logm('Connection timed out...');
                close($sock);
                undef(@queue);
                undef($sock);
                sleep(3);
                goto CONNECT;
                return;
        }
}

# Parses an incomming message
# parse("message")
sub parse
{
        my ($data) = @_;
        my ($type, $remaining, $msg, $arenaname, $freq, $nick, $ship, $msgtype, $cmd, $arg, $channelnum, $squad, $money);
        
        logm("<< $data") if $config{log_raw};
        
        ($type, $remaining) = split(':', $data, 2);
        $type = uc($type);
        
        if ($type eq 'LOGINBAD') 
        {       
                # LOGINBAD:message
                $msg = $remaining;
                
                logm("Unable to connect: $msg");
                
                # Retry
                sleep($config{server_reconnectdelay});
                goto CONNECT;
        }
        elsif ($type eq 'LOGINOK')  
        { 
                $msg = $remaining;
                # LOGINOK:yourname
                $bot_nick = $msg if ($msg && length($msg) > 0);
                        
                sts("GO:$config{server_arena}");
        }
        elsif ($type eq 'INARENA') 
        {
                #INARENA:arenaname:freq
                ($arenaname, $freq) = split(':', $remaining, 2);
                
                if (length($config{bot_chat}) > 0) 
                {
                          sts("SEND:CMD:?chat=$config{bot_chat}");
                }
        }
        elsif ($type eq 'PLAYER')
        {
                # SHIPFREQCHANGE:nick:ship:freq
                ($nick, $ship, $freq) = split(':', $remaining, 3);
        }
        elsif ($type eq 'ENTERING')
        {
                # ENTERING:nick:ship:freq
                ($nick, $ship, $freq) = split(':', $remaining, 3);
        }
        elsif ($type eq 'LEAVING')
        {
                # LEAVING:nick
                $nick = $remaining;
        }
        elsif ($type eq 'SHIPFREQCHANGE')
        {
                # SHIPFREQCHANGE:nick:ship:freq
                ($nick, $ship, $freq) = split(':', $remaining, 3);
        }
        elsif ($type eq 'MSG')
        {
                $msgtype = '';
                ($msgtype, $remaining) = split(':', $remaining, 2);
                $msgtype = uc($msgtype);
                
                if ($msgtype eq 'ARENA')
                {
                        # MSG:ARENA:msg
                        $msg = $remaining;
                        
                        if ($msg =~ /^Player (.+?) gave you \$(\d+)\.(?: Message: \"(.+)\")?$/) #"
                        {
                                $nick = $1;
                                $money = $2;
                                $msg = (defined($3) ? $3 : '');
                                
                                #...
                        }
                }
                elsif ($msgtype eq 'CMD')
                {
                        # MSG:CMD:msg
                        $msg = $remaining;
                }
                elsif ($msgtype eq 'PUB')
                {
                        # MSG:PUB:nick:msg
                        ($nick, $msg) = split(':', $remaining, 2);
                }
                elsif ($msgtype eq 'PUBM')
                {
                        # MSG:PUBM:nick:msg
                        ($nick, $msg) = split(':', $remaining, 2);
                }
                elsif ($msgtype eq 'PRIV')
                {
                        # MSG:PRIV:nick:msg
                        ($nick, $msg) = split(':', $remaining, 2);
                        
                        $msg =~ s/^[!\.]//;
                        ($cmd, $arg) = split(/\s+/, $msg, 2);
                        
                        return if (lc($nick) eq lc($bot_nick));
                        
                        command($nick, 1, lc($cmd), $arg);
                }
                elsif ($msgtype eq 'FREQ')
                {
                        # MSG:FREQ:nick:msg
                        ($nick, $msg) = split(':', $remaining, 2);
                }
                elsif ($msgtype eq 'CHAT')
                {
                        # MSG:CHAT:channelnum:nick> msg
                        ($channelnum, $remaining) = split(':', $remaining, 2);
                        ($nick, $msg) = split('> ', $remaining, 2);
                }
                elsif ($msgtype eq 'MOD')
                {
                        # MSG:MOD:nick:msg
                        ($nick, $msg) = split(':', $remaining, 2);
                }
                elsif ($msgtype eq 'SYSOP')
                {
                        # MSG:SYSOP:nick:msg
                        ($nick, $msg) = split(':', $remaining, 2);
                }
                elsif ($msgtype eq 'SQUAD')
                {
                        # MSG:SQUAD:squad:sender:msg
                        ($squad, $nick, $msg) = split(':', $remaining, 3);
                }
        }

}

# Parse a command sent by a user
# command("JoWie", 1, lc("die"), "be right back")
sub command
{
        my ($nick, $private, $command, $args) = @_;
        my $ha = ha($nick); # has admin?
 
        if ($command eq 'ping')
        {
        }
        elsif ($command eq 'raw' && $ha)
        {
                if ($args)
                {
                        sts($args);
                }
                else
                {
                        privmsg($nick, "Try: RAW <chatnet command>");
                }
        }
        elsif ($command eq 'die' && $ha)
        {
                close($sock);
                undef(@queue);
                undef($sock);
                die("die from $nick");
        }
        elsif ($command eq 'restart' && $ha)
        {
                close($sock);
                undef(@queue);
                undef($sock);
                exec("perl \"$0\"");
                die ("RESTART from $nick failed!");
        }
}

sub privmsg
{
        my ($nick, $msg) = @_;
        while (length($msg)) 
        {
                sts("SEND:PRIV:$nick:".substr($msg,0, ASSS_MAXMESSAGELENGTH));
                substr($msg, 0, ASSS_MAXMESSAGELENGTH) = "";
        }
}

# Send a chatnet command to the server
# sts("message", 0)
sub sts
{
        my($message, $skipq) = @_;
        
        if ($skipq)
        {
                if ($sock) 
                {
                        print $sock "$message\r\n";
                        $lastmessage_sent = time();
                        logm(">> $message") if $config{log_raw};
                }
                else 
                {
                        # Something is wrong. the socket is closed. clear the queue
                        undef(@queue);
                        logm("\$sock isn't writeable in sts(), cleared outgoing queue.\n");
                        return;
                }
        }
        else
        {
                push(@queue, $message);
                
                if ($config{maxqueue} && @queue > $config{config})
                {
                        undef(@queue);
                        logm("Outgoing message queue cleared.");
                }
        }
}

# Deliver messages from queue, this should be called every X seconds
# fq()
sub fq
{
        $queue_lasttime = time();
        for (0..$config{queue_lines})
        {
                last() if !@queue; # no messages left to send
                my $line = shift(@queue); # Get the next message
                
                if ($sock)
                {       
                        print $sock "$line\r\n";
                        $lastmessage_sent = time();
                        logm(">> $line") if $config{log_raw};
                }
                else 
                {
                        undef(@queue);
                        logm("Disconnected: cleared outgoing message queue.");
                        last();
                }
        }
}

# Does the user have admin access
# ha("JoWie") == 1
sub ha
{
        my ($nick) = @_;
        
        if (length($config{admin_nick}) > 0 && lc($nick) eq lc($config{admin_nick}))
        {
                return 1;
        }
        
        return 0;
}

# timestamp
sub ts 
{ 
        my @ts = localtime(time());
        return sprintf("[%02d/%02d/%02d %02d:%02d:%02d] ", $ts[3],$ts[4]+1,$ts[5]%100,$ts[2],$ts[1],$ts[0]);
}

# Logs a message to the log file / console
# logm("message")
my $logHandle = 0;
sub logm
{
        my ($msg) = @_;
        
        print ts().$msg."\n";
        
        if ($config{log_file})
        {
        	unless ($logHandle)
        	{
	                open($logHandle, ">>".$config{log_file}) or do
	                {
	                        return $msg;
	                };
                }
                print $logHandle ts().$msg."\n";
        }
        
        return $msg;
}

# Trims leading and trailing spaces
# trim("string")
sub trim
{
        my $string = shift;
        $string =~ s/^\s+//;
        $string =~ s/\s+$//;
        return $string;
}

# Clean up and quit
# sigint()
sub sigint
{
        close($sock) if ($sock);
        undef(@queue);
        undef($sock);
        logm("Quiting, received signal");
        close($logHandle) if ($logHandle);
        die();
}