<?php
    if (empty($_GET['player']))
    {
        require('players.php');
        die();
    }
    
    require_once("config.php");
    require_once("commonfunctions.php");
    
    $player = substr($_GET['player'],0,100);
    
    #if ($player=="") header('Location: http://'.$_SERVER['SERVER_NAME'].
    #    ($_SERVER['SERVER_PORT']!=80?':'.$_SERVER['SERVER_PORT']:'').$baseurl.
    #    'players.php');
    
    $page_title = "Player Info: " . htmlentities($player);
    $page_id = "Player Info";
    require("header.php");
    
    $showmap = (isset($_GET['showmap']) ? $_GET['showmap'] : 0);
    $allmods = (isset($_GET['allmods']) ? $_GET['allmods'] : 0);
    
    echo "<h1>Player Info</h1>";
    $file = fopen($irpg_db,"r");
    fgets($file,1024); // skip top comment
    $found=0;
    
    $online = 0;
    
    # username	pass	is admin	level	class	ttl	next ttl	nick	logged in	online	idled	x pos	y pos	
    # created	last seen	amulet	charm	helm	boots	gloves	ring	leggings	shield	tunic	weapon	alignment	stat_combat	stat_strength	stat_endurance	stat_points
    while ($line=fgets($file,1024)) 
    {
        if (substr($line,0,strlen($player)+1) == $player."\t") {
            list($user,,$isadmin,$level,$class,,$ttl,$nick,$loggedin,$online,$idled,
                 $x,$y,
                 $created,
                 ,
                 $lastactivity,
                 $item['amulet'],
                 $item['charm'],
                 $item['helm'],
                 $item['boots'],
                 $item['gloves'],
                 $item['ring'],
                 $item['leggings'],
                 $item['shield'],
                 $item['tunic'],
                 $item['weapon'],
                 $alignment,
                 ,
                 $stat_combat,
                 $stat_strength,
                 $stat_endurance
                ) = explode("\t",trim($line));
            $found = 1;
            break;
        }
    }
    if (!$found) echo "<h1>Error</h1><p><b>No such user.</b></p>\n";
    else {
        $class=htmlentities($class);
        /* if we htmlentities($user), then we cannot use links with it. */
        echo "      <p><b>User:</b> ".htmlentities($user)."<br />\n".
             "      <b>Class:</b> $class<br />\n".
             "      <b>Subspace Nick:</b> ".htmlentities($nick)."<br />\n".
             ($lastactivity ? "      <b>Last Seen:</b> ". duration(time() - $lastactivity) . "<br />\n" : "").
             "      <b>Level:</b> $level<br />\n".
             "      <b>Next level:</b> ".duration($ttl)."<br />\n".
             "      <b>Status: </b> O".($online?"n":"ff")."line<br />\n".
             "      <b>Account Created:</b> ".date("D M j H:i:s Y",$created)."<br />\n".
             "      <b>Total time idled:</b> ".duration($idled)."<br />\n".
             "      <b>Current position:</b> [$x,$y]<br />\n".
             "      <b>Alignment:</b> ".($alignment=='e'?"Evil":($alignment=='n'?"Neutral":"Good"))."<br />\n".
             "      <b>Combat Level:</b> $stat_combat<br />\n".
             "      <b>Strength Level:</b> $stat_strength<br />\n".
             "      <b>Endurance Level:</b> $stat_endurance<br />\n".
             "      <b>XML:</b> [<a href=\"xml.php?player=".urlencode($user)."\">link</a>]</p>\n".
             "    <h2>Map</h2>\n".
             "    ".($showmap?"<div id=\"map\"><img src=\"makemap.php?player=".urlencode($user)."\"></div>\n\n":"<p><a href=\"?player=".urlencode($user)."&amp;showmap=1\">Show map</a></p>\n\n")."".
             "    <h2>Items</h2>\n<p>";
        ksort($item);
        $sum = 0;
        foreach ($item as $key => $val) 
        {
            if (substr($val,-1,1) == "a") {
                $val = intval($val)." [<span class=\"specialitem\">Joris' Omniscience Grand Crown</span>]";
            }
            if (substr($val,-1,1) == "b") {
                $val = intval($val)." [<span class=\"specialitem\">Hat's Protectorate Electronic Mail</span>]";
            }
            if (substr($val,-1,1) == "c") {
                $val = intval($val)." [<span class=\"specialitem\">CDB's Storm Magic Amulet</span>]";
            }
            if (substr($val,-1,1) == "d") {
                $val = intval($val)." [<span class=\"specialitem\">Irvel's Furry Colossal Sword</span>]";
            }
            if (substr($val,-1,1) == "e") {
                $val = intval($val)." [<span class=\"specialitem\">Neostar's Cane of Blind Rage</span>]";
            }
            if (substr($val,-1,1) == "f") {
                $val = intval($val)." [<span class=\"specialitem\">Hi-5's Magical Boots of Swiftness</span>]";
            }
            if (substr($val,-1,1) == "g") {
                $val = intval($val)." [<span class=\"specialitem\">JoWie's Cluehammer of Doom</span>]";
            }
            if (substr($val,-1,1) == "h") {
                $val = intval($val)." [<span class=\"specialitem\">Beam's Glorious Ring of Sparkliness</span>]";
            }
            if (substr($val,-1,1) == "i") {
                $val = intval($val)." [<span class=\"specialitem\">Noldec' Silk Skirt of Ancient Magick</span>]";
            }
            if (substr($val,-1,1) == "j") {
                $val = intval($val)." [<span class=\"specialitem\">Jokerd3's Charm of Blight</span>]";
            }
            if (substr($val,-1,1) == "k") {
                $val = intval($val)." [<span class=\"specialitem\">IdleRPG's Magic Cheat</span>]";
            }
            if (substr($val,-1,1) == "m") {
                $val = intval($val)." [<span class=\"specialitem\">Stone of Jordan</span>]";
            }
            echo "      $key: $val<br />\n";
            $sum += $val;
        }
        echo "      <br />\n      sum: $sum<br />\n</p>";
        
                
        $file = fopen($irpg_mod, "r");
        
        $temp = array();
        while ($line = fgets($file,1024)) 
        {
            if (strstr($line," ".$player." ") ||
                strstr($line," ".$player.", ") ||
                substr($line,0,strlen($player)+1) == $player." " ||
                substr($line,0,strlen($player)+3) == $player."'s ") 
            {
                array_push($temp,$line);
            }
        }
        fclose($file);
        
        if (!is_null($temp) && count($temp)) 
        {
            echo('<h2>');
            echo $allmods != 1 ? "Recent ":"";
            echo('Character Modifiers</h2><p>');
            if ($allmods == 1 || count($temp) < 6) {
                foreach ($temp as $line) {
                    $line=htmlentities(trim($line));
                    echo "      $line<br />\n";
                }
                echo "      <br />\n";
            }
            else {
                end($temp);
                for ($i=0;$i<4;++$i) prev($temp);
                for ($line=trim(current($temp));$line;$line=trim(next($temp))) {
                    $line=htmlentities(trim($line));
                    echo "      $line<br />\n";
                }
            }
        }
        if ($allmods != 1 && count($temp) > 5) {
?>
      <br />
      [<a href="<?php echo $_SERVER['PHP_SELF']."?player=".urlencode($user);?>&amp;allmods=1">View all Character Modifiers</a> (<?php echo count($temp)?>)]
      </p>
<?php
        }
    }
    require("footer.php");  
?>