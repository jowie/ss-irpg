<?php
    require_once("config.php");
    require_once("commonfunctions.php");
    $page_title = "Player Info";
    $page_id = "Player Info";
    require("header.php");
?>
    <h1>Players</h1>
    <p>See player stats in <a href="db.php">table format</a>.</p>
    <h2>Pick a player to view</h2>
    <ol>
<?php
    $file = file($irpg_db);
    unset($file[0]);
    usort($file, 'cmp_level_desc');
    $unlisted = 0;
    foreach ($file as $line) 
    {
    	//username	pass	is admin	level	class	ttl	next ttl	nick	logged in	online	idled	x pos	y pos	created	last seen	amulet	charm	helm	boots	gloves	ring	leggings	shield	tunic	weapon	alignment	stat_combat	stat_strength	stat_endurance	stat_points	duelpoints	nextbonusduelpoint
        list($user,,,$level,$class,,$secs,,,$online) = explode("\t",trim($line));
	if ($level == 0)
	{
		$unlisted++;
		continue;
	}
        $class = htmlentities($class);
        $next_level = duration($secs);

        print "        <li".(!$online?" class=\"offline\"":"")."><a".
              (!$online?" class=\"offline\"":"").
              " href=\"playerview.php?player=".urlencode($user).
              "\">".htmlentities($user).
              "</a>, the level $level $class. Next level in $next_level.<!--$secs--></li>\n";

    }
    
?>
    </ol>

<?php 
    if ($unlisted)
    	echo "<p>$unlisted more players not listed (level 0)</p>\n";
require("footer.php"); ?>