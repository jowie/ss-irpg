<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Subspace Idle Role Playing Game: <?php print $page_title; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="idlerpg.css" />
  </head>
  <body>

    <div class="head">
        <img src="idlerpg.png" alt="Idle RPG" title="Idle RPG" width="338" height="115" />
    </div>

    <div id="menu" class="menu">

<?php
    $topbarurl = array(
        'Game Info' => $baseurl . '',
        'Player Info' => $baseurl . 'players.php',
        'World Map' => $baseurl . 'worldmap.php',
        'Source code!' => 'https://bitbucket.org/jowie/ss-irpg/src',
        'Original IdleRPG' => 'http://www.idlerpg.net/',
    );

    foreach ($topbarurl as $key => $value) 
    {
        if ($key ==  $page_id) 
        {
            echo "        <a class=\"current\" href=\"$value\">$key</a>\n";
        }
        else {
            echo "        <a href=\"$value\">$key</a>\n";
        }
    }
?>
    </div>

    <div class="content">