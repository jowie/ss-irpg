<?php
        require_once("config.php");
        
        $page_title = "World Map";
        $page_id = "World Map";
        
        require("header.php");
?>

<h1>World Map</h1>
<div id="map">
    <img src="makeworldmap.php" alt="IdleRPG World Map" title="IdleRPG World Map" usemap="#world"/>
    <map id="world" name="world">
<?php
    $file = fopen($irpg_db,"r");
    $xfac = 500 / $mapx;
    $yfac = 500 / $mapy;
    fgets($file);
    while($location=fgets($file)) 
    {
        list($who,,,$level,,,,,,,,$x,$y) = explode("\t",trim($location));
        if ($level == 0) continue;
        $x = (int)($x * $xfac);
        $y = (int)($y * $yfac);
        print "        <area shape=\"circle\" coords=\"".$x.",".$y.",5\" alt=\"".htmlentities($who).
              "\" href=\"playerview.php?player=".urlencode($who)."\" title=\"".htmlentities($who)."\" />\n";
    }
    fclose($file);
?>
    </map>
</div>
<p>[offline users are red, online users are blue]</p>

<?php require("footer.php"); ?>