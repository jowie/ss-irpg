<?php
    require_once("config.php");
    $page_title = "Game Info";
    $page_id = "Game Info";
    require("header.php");
?>
    <h1>Game Info</h1>
    <p>The Idle RPG is just what it sounds like: an RPG in which the players
    idle. In addition to merely gaining levels, players can find items and
    battle other players. However, this is all done for you; you just idle.
    There are no set classes; you can name your character anything you like, and
    have its class be anything you like, as well.</p>

    <h2>Location</h2>
    <p> 
      The Idle RPG can be played in SSCJ Devastation
    </p>
    

    <h2 id="register">Registering</h2>
    
      <p>To register, simply:</p>
      
        <code>
          :IdleRPG:REGISTER char name:recovery password:char class
        </code>
      
      <p>Where 'char_name' can be up to 16 chars long and 'char class' can be up to 30 chars. Everything may contain spaces</p>
      <p>When you register, your IdleRPG name is bound to your subspace name, 
      if you want to play from a different name you will have to use the following command:</p>
      <code>
        :IdleRPG:RECOVER char name:recovery password
      </code>
      
    <h2 id="login">Logging In</h2>
    
      <p>To login, simply:</p>
      
        <code>
          :IdleRPG:LOGIN
        </code>
    

    <h2 id="logout">Logging Out</h2>
    
      <p>To logout, simply:</p>
      
        <code>
          :IDLERPG:LOGOUT
        </code>
    

    <h2 id="newpass">Changing Your Password</h2>
    
      <p>To change your password, simply:</p>
      
        <code>
          :IdleRPG:NEWPASS old password:new password
        </code>
      
      <p>If you have forgotten your password, please use the <a href="#info">
      INFO</a> command to find an online admin to help you.</p>
    

    <h2 id="removeme">Removing Your Account</h2>
    
      <p>To remove your account, simply:</p>
      
        <code>
          :IdleRPG:REMOVEME password
        </code>
    

    <h2 id="alignment">Changing Your Alignment</h2>
    
      <p>To change your alignment, simply:</p>
      
        <code>
          :IdleRPG:ALIGN good|neutral|evil
        </code>
      
      <p>Your alignment can affect certain aspects of the game. You may align
      with good, neutral, or evil. 'Good' users have a 10% boost to their item
      sum for battles, and a 1/12 chance each day that they, along with a
      'good' friend, will have the light of their god shine upon them,
      accelerating them 5-12% toward their next level. 'Evil' users have a 10%
      detriment to their item sum for battles (ever forsaken in their time of
      most need...), but have a 1/8 chance each day that they will either a)
      attempt to steal an item from a 'good' user (whom they cannot help but
      hate) or b) be forsaken (for 1-5% of their TTL) by their evil god. After
      all, we all know that crime doesn't pay. Also, 'good' users have only a
      1/50 chance of landing a <a href="#critstrike">Critical Strike</a> when
      battling, while 'evil' users (who always fight dirty) have a 1/20
      chance. Neutral users haven't had anything changed, and all users start
      off as neutral.</p>
      <p>I haven't run the numbers to see which alignment it is better to
      follow, so the stats for this feature may change in the future.</p>
    
    
      <h2 id="info">Obtaining Bot Info</h2>
    
      <p>To see some simple information on the bot, simply:</p>
      
        <code>
          :IdleRPG:INFO
        </code>
      
      <p>This command gives info such as to which server the bot is connected
      and the nicknames of online bot admins.</p>
    
    

    <h2 id="levelling">Levelling</h2>
    
      <p>To gain levels, you must only be logged in and idle. The time
      between levels is based on your character level, and is calculated
      by the formula:</p>
      
        <code>TTL = 300 * (1.16 ^ max(YOUR_LEVEL, 50)) + min(YOUR_LEVEL - 50, 0) * 86400</code>    

    <h2 id="quest">Checking the Active Quest</h2>
    
      <p>To see the active quest, its users, and its time left to
      completion:</p>
      
        <code>
          :IDLERPG:QUEST
        </code>
      
    

    <h2 id="playerinfo">Player information</h2>
    
      <p>To see whether you are logged on, simply:</p>
      
        <code>
          :IdleRPG:WHOAMI
        </code>
      
      <p>To find out extensive information about yourself or another user, use:</p>
        <code>
          :IdleRPG:STATUS<br />
          :IdleRPG:STATUS character name
        </code>
    
      <p>To find out the character name for a subspace player, use</p>
        <code>
          :IdleRPG:FINDUSER subspace nickname
        </code>
    

    <h2 id="item">Items</h2>
    
      <p>Each time you level, you find an item. You can find an item as
      high as 1.5*YOUR_LEVEL (unless you find a <a href="#uniqueitems">
      unique item</a>). There are 10 types of items: rings,
      amulets, charms, weapons, helms, tunics, gloves, leggings,
      shields, and boots. You can find one of each type. When you find
      an item with a level higher than the level of the item you already
      have, you toss the old item and start using the new one.</p>
      <p> You can view the items you have using:</p>
      <code>
        :IdleRPG:ITEMS
      </code>

      <p>As you may guess, you have a higher chance of rolling an item of a
      lower value than you do of rolling one of a higher value level. The exact
      formula is as follows:</p>

      <code>
           for each 'number' from 1 to YOUR_LEVEL*1.5<br />
           &nbsp;&nbsp;you have a 1 / ((1.4)^number) chance to find an
           item at this level<br />
           end for
      </code>

      <p>As for item type, you have an equal chance to roll any type.</p>
    


    <h2 id="battle">Battle</h2>
    
      <p>Each time you level, you will challenge someone. A pool of
      opponents is chosen of all online players, and one is chosen randomly. If
      there are no other online players, you will fight the bot. 
      This is how the victor is decided:</p>

      <ul>
          <li>Your item levels are summed.</li>
          <li>Their item levels are summed.</li>
          <li>Your minimum and maximum roll is calculated based 
          on your <a href="#stats">stats</a> and item sum</li>
          <li>Their minimum and maximum roll is calculated based 
          on your <a href="#stats">stats</a> and item sum</li>
          <li>A random number between minimum and your maximum is taken.</li>
          <li>A random number between minimum and their maximum is taken.</li>
          <li>If your roll is larger than theirs, you win.</li>
      </ul>

      <p>If you win, your time towards your next level is lowered. The amount
      that it is lowered is based on your opponent's level. The formula is:</p>

      
         ((the larger number of (OPPONENT_LEVEL/4) and 7) / 100) *
         YOUR_NEXT_TIME_TO_LEVEL
      

      <p>This means that you lose no less than 7% from your next time to level.
      If you win, your opponent is not penalized any time, unless you land a
      <a href="#critstrike">Critical Strike</a>.</p>

      <p>If you lose, you will be penalized time. The penalty is calculated
      using the formula:</p>

      
        ((the larger number of (OPPONENT_LEVEL/7) and 7) / 100) *
        YOUR_NEXT_TIME_TO_LEVEL
      

      <p>This means that you gain no less than 7% of your next time to level.
      If you lose, your opponent is not awarded any time.</p>

      <p>Battling the IRPG bot is a special case. The bot has an item sum of
      1+[highest item sum of all players]. The percent awarded if you win is a
      constant 20%, and the percent penalized if you lose is a constant 10%.</p>

      <p>If more than 15% of online players are level 45 or
      higher, then a random level 45+ user will battle another random player
      every hour. This is to speed up levelling among higher level players.</p>

      <p>There is a grid system. The grid is a 500 x 500
      area in which players may walk. If you encounter another player on the
      grid, you have a 1 / (NUMBER_OF_ONLINE_PLAYERS) chance to battle them.
      Battle awards are calculated using the above formulae. More information
      on the grid system is available <a href="#grid">here</a>.</p>
      
      <p>A successful battle may result an item being
      <a href="#stealing">stolen</a>.</p>
    
    
    <a name="uniqueitems"></a><h2>Unique Items</h2>
    
      <p>After level 25, you have a chance to roll items
      significantly higher than items you would normally find at that level.
      These are unique items, and have the following properties:</p>
      <table id="uniques" class="uniques">
        <tr>
          <th>Name</th>
          <th>Item Level Range</th>
          <th>Required User Level</th>
          <th>Chance to Roll</th>
        </tr>
        <tr>
        <!-- a -->
          <th>Joris' Omniscience Grand Crown</th>
          <td>50-74</td>
          <td>25+</td>
          <td>1 / 40</td>
        </tr>
        <tr>
        <!-- b -->
          <th>Hat's Protectorate Electronic Mail</th>
          <td>75-99</td>
          <td>30+</td>
          <td>1 / 40</td>
        </tr>
        <tr>
        <!-- c -->
          <th>CDB's Storm Magic Amulet</th>
          <td>100-124</td>
          <td>35+</td>
          <td>1 / 40</td>
        </tr>
        <tr>
        <!-- d -->
          <th>Irvel's Furry Colossal Sword</th>
          <td>150-174</td>
          <td>40+</td>
          <td>1 / 40</td>
        </tr>
        <tr>
        <!-- e -->
          <th>Neostar's Cane of Blind Rage</th>
          <td>175-200</td>
          <td>45+</td>
          <td>1 / 40</td>
        </tr>
        <tr>
        <!-- f -->
          <th>Hi-5's Magical Boots of Swiftness</th>
          <td>250-300</td>
          <td>48+</td>
          <td>1 / 40</td>
        </tr>
        <tr>
        <!-- g -->
          <th>JoWie's Cluehammer of Doom</th>
          <td>300-350</td>
          <td>52+</td>
          <td>1 / 40</td>
        </tr>
        <tr>
        <!-- h -->
          <th>Beam's Glorious Ring of Sparkliness</th>
          <td>50-74</td>
          <td>25+</td>
          <td>1 / 40</td>
        </tr>
        <tr>
        <!-- i -->
          <th>Noldec' Silk Skirt of Ancient Magick</th>
          <td>200-300</td>
          <td>38+</td>
          <td>1 / 40</td>
        </tr>
        <tr>
        <!-- j -->
          <th>Jokerd3's Charm of Blight</th>
          <td>150-200</td>
          <td>35+</td>
          <td>1 / 40</td>
        </tr>
        <tr>
        <!-- k -->
          <th>IdleRPG's Magic Cheat</th>
          <td>???</td>
          <td>???</td>
          <td>???</td>
        </tr>
        <tr>
        <!-- m -->
          <th>Stone of Jordan</th>
          <td>150-250</td>
          <td>0</td>
          <td>1 / 100</td>
        </tr>
        
      </table>
    
    <h2>The Hand of God</h2>
    
      <p>Every online user has a (roughly) 1/20 chance per day
      of a "Hand of God" affecting them. A HoG can help or hurt your character
      by carrying it between 5 and 75 percent towards or away from its next time
      to level. The odds are in your favor, however, with an 80% chance to help
      your character, and only a 20% chance of your character being smitten.</p>

      <p>In addition to occurring randomly, admins may summon the HoG at their
      whim.</p>
    

      <h2 id="critstrike">Critical Strike</h2>
    
      <p>If a challenger beats his opponent in battle, he has a
      1/35 chance of landing a Critical Strike. If this occurs, his opponent
      is penalized time towards his next time to level. This amount is
      calculated by the formula:</p>
      
        ((random number from 5 to 25) / 100) * OPPONENT'S_NEXT_TIME_TO_LEVEL
      
      <p>Meaning he gains no less than 5% and no more than 25% of his next time
      to level.</p>
    

    <h2>Team Battles</h2>
    
      <p>Every online user has (roughly) 1/4 chance per day of
      being involved in a  'team battle.' Team battles pit three online
      players against three other online players. Each side's items are summed,
      and a winner is chosen as in regular battling. If the first group bests
      the second group in combat, 20% of the lowest of the three's TTL is
      removed from their clocks. If the first group loses, 20% of their lowest
      member's TTL is added to their TTL.</p>
    

    <h2>Calamities</h2>
    
      <p>Every online user has a (roughly) 1/8 chance per day of a
      calamity occurring to them. A calamity is a bit of extremely bad luck that
      either:<br />
      
          a) slows a player 5-12% of their next time to level<br />
          b) lowers one of their item's value by 10%
      
      </p>
    

    <h2>Godsends</h2>
    
      <p>Every online user has a (roughly) 1/8 chance per day of a
      godsend occurring to them. A godsend is a bit of extremely good luck that
      either:<br />
      
          a) accelerates a player 5-12% of their next time to level<br />
          b) increases one of their item's value by 10%
      
      </p>
    

    <h2>Quests</h2>
    
      <p>Four level 40+ users that have been
      online for more than 5 hours are chosen to represent and assist the
      Realm by going on a quest. If all four users make it to the quest's end,
      all questers are awarded by removing 25% of their TTL (ie, their TTL at
      quest's end). To complete a quest, no user can be penalized until the
      quest's end. As of v3.0, there are two kinds of quests: grid-based quests
      and time-based quests. Time-based quests last a random time between 12 and
      24 hours. Grid-based quests are based on the <a href="#grid">grid
      system</a> and do not have a set time to completion. Rather, the questers
      must reach certain points on the map for their quest to be complete. If
      the quest is not completed, ALL online users are penalized a p15 as
      punishment.</p>
    
    <h2 id="grid">Grid System</h2>
    
      <p>The IRPG has a grid system. The grid can be considered
      a 500 x 500 point map on which the players may walk. Every second, each
      player has an equal chance to step up, down, or neither, and an equal
      chance to step left, right, or neither. If a user encounters another
      player, there is a 1/(NUMBER_OF_ONLINE_PLAYERS) chance that they will
      battle one another. Normal battling rules apply.</p>
      
      <p>Some quests require that users walk to certain points on the map. In
      the spirit of IRPG, of course, the trek is made for you. Your character
      will automatically walk in the direction that it is supposed to, although
      at a much slower than normal pace (to avoid accidents, of course. you
      don't want to fall down and risk a Realm-wide p15!).</p>
    
    
    <h2 id="stealing">Item Stealing</h2>
    
      <p>The IRPG has item stealing. After each battle, if the
      challenger wins, he has a slightly less than 2% chance of stealing an
      item from the challengee. Only items of a higher value are stolen, and
      the challenger's old item is given to the challengee in a moment of pity.
      </p>
    
    <h2 id="stats">Stats</h2>
      <p>The Subspace version of IRPG has introduced the stats system. When you 
      register with IRPG you get 10 stat points, after that you get 1 per level.</p>
      <p>You can spend your stats by using the following command:</p>
      <code>
        :IdleRPG:ADDSTAT combat<br />
        :IdleRPG:ADDSTAT strength 5<br />
        :IdleRPG:STATRESET
      </code>

      <p><strong>Combat</strong> determines the minimum you will roll when batteling. Each 
      combat level will add a minimum roll of 2% of your itemsum.
      </p>
      <p><strong>Strength</strong> determines the maximum you will roll when batteling. Each 
      strength level will add a maximum roll of 2% of your itemsum. 
      Strength will not effect your minimum roll.
      </p>
      <p><strong>Endurance</strong> will decrease the time it takes to level by 1% per endurance level.</p>
      
      <p>The maximum you can spend on a stat is 50 points</p>
   <h2 id="duel">Dueling</h2>
      <p>Players above level 15 may duel other players up to 8 times. When you level up you are able to duel 4 times and once very 6 hours. Dueling has a large chance of hitting a critical strike.</p>
      <p>To duel a player, use:</p>
      <code>
      	:IdleRPG:DUEL someplayer
      </code>

    <h2>Credits</h2>
      <p>IdleRPG modified for Subspace Chatnet by JoWie, original credits:</p>
      <p>Many thanks to version 3.0's map creators, dreamisle, res0 and Jeb!
	  The game wouldn't be the same without you.</p>
      <p>
        The IRPG would not be possible without help from a lot of people.
        To jwbozzy, yawnwraith, Tosirap, res0, dwyn, Parallax, protomek,
        Bert, clavicle, drdink, jeff, rasher, Sticks, Nerje, Asterax,
        emad, inkblot(!), schmolli, mikegrb, mumkin, sean, Minhiriath,
        and Dan, I give many thanks. Unfortunately, this list has grown too
        large to maintain. More user contributions can be seen in the
        original <a href="http://idlerpg.net/ChangeLog.txt">ChangeLog</a>.
      </p>
<?php require("footer.php"); ?>