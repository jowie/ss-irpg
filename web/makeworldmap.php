<?php
    require_once("config.php");
    
    session_start(); // sessions to generate only one map / person / 3s
    if (isset($_SESSION['time']) && time()-$_SESSION['time'] < 3) 
    {
        header("Location: maperror.png");
        exit(0);
    }
    $_SESSION['time'] = time();
    
    $file = fopen($irpg_db,"r");
    fgets($file);

    $xfac = 500 / $mapx;
    $yfac = 500 / $mapy;
    $map = imageCreate(500,500);
    $magenta = ImageColorAllocate($map, 255, 0, 255);
    $blue = imageColorAllocate($map, 0, 128, 255);
    $red = imageColorAllocate($map, 211, 0, 0);
    ImageColorTransparent($map, $magenta);
    while ($line=fgets($file)) 
    {    
        list(,,,$level,,,,,,$online,,$x,$y) = explode("\t",trim($line));
        if ($level == 0) continue;
        $x = (int)($x * $xfac);
        $y = (int)($y * $yfac);
        
        if ($online == 1) imageFilledEllipse($map, $x, $y, 5, 5, $blue);
        else imageFilledEllipse($map, $x, $y, 5, 5, $red);
    }
    header("Content-type: image/png");
    imagePNG($map);
    imageDestroy($map);
?>